class Performance < ActiveRecord::Base
  has_many :showings, :dependent => :destroy
  has_many :pieces, :through => :showings
end

# == Schema Information
#
# Table name: performances
#
#  id               :integer(4)      not null, primary key
#  location_id      :integer(4)
#  performance_date :datetime
#  title            :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

