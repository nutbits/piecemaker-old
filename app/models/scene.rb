# == Schema Information
#
# Table name: scenes
#
#  id            :integer(4)      not null, primary key
#  title         :string(255)
#  assemblage_id :integer(4)
#  created_at    :datetime
#  updated_at    :datetime
#

class Scene < ActiveRecord::Base
  belongs_to :assemblage
  has_many :blocks

  def non_cloned_blocks #tested
    @ncb ||= self.blocks.reject{|x| x.is_clone}
  end
  def start_time #tested
    self.non_cloned_blocks.length > 0 ? self.non_cloned_blocks.sort_by{|x| x.start_time}.first.start_time : nil
  end
  def end_time #tested
    self.non_cloned_blocks.length > 0 ? self.non_cloned_blocks.sort_by{|x| x.end_time}.last.end_time : nil
  end
  def all_my_scenes #tested
    @@amc ||= self.assemblage.scenes.reject{|x| !x.start_time}.sort_by{|x| x.start_time}
  end
  def z_index #tested
    self.get_z_index
  end
  def get_z_index #tested
    z = 100
    all_scenes = all_my_scenes.reject{|x| x.id == self.id}
    answer = all_scenes.select{|x| x.end_time > self.start_time && x.start_time < self.start_time}
    if answer.length > 0
      z = answer.last.z_index + 1
    end
    z
  end
  def cast #tested
    cast = []
    self.blocks.each do |block|
      cast += block.cast if block.cast
    end
    cast.uniq.sort
  end
  def duration #tested
    @duration ||= self.blocks.length > 0 ? end_time - start_time : nil
  end
  def self.scenes_for(grouped_blocks) #tested
    scenes = []
    grouped_blocks.each do |block|
      scenes << block.scene if block.scene
    end
    sc = scenes.uniq
  end
  def scene_number #tested
    all_scenes = self.assemblage.scenes.select{|x| x.start_time}.sort_by{|x| x.start_time}
    scene_number = all_scenes.index(self) + 1
  end
  def self.scene_id_for_block(block) #tested
    answer = nil
    scenes = block.assemblage.scenes
    scenes.each do |scene|
      if (scene.start_time && block.start_time >= scene.start_time && block.start_time <= scene.end_time
        )
        answer = scene
      end
    end
    if answer
      answer = answer.id
    end
    answer
      
  end
end
