class Showing < ActiveRecord::Base
  belongs_to :piece
  belongs_to :performance
end

# == Schema Information
#
# Table name: showings
#
#  id             :integer(4)      not null, primary key
#  piece_id       :integer(4)
#  performance_id :integer(4)
#

