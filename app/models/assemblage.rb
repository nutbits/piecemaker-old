# == Schema Information
#
# Table name: assemblages
#
#  id             :integer(4)      not null, primary key
#  piece_id       :integer(4)
#  created_at     :datetime
#  created_by     :string(255)
#  block_list     :text
#  title          :string(255)
#  piece_duration :integer(4)      default(3600)
#  pre_roll       :integer(4)      default(0)
#  start_time     :integer(4)      default(0)
#  post_roll      :integer(4)      default(0)
#  grid           :boolean(1)      default(TRUE)
#  overlap_check  :boolean(1)      default(FALSE)
#  constrained    :boolean(1)      default(FALSE)
#  warning        :boolean(1)      default(FALSE)
#  track_cues     :boolean(1)      default(TRUE)
#  track_undos    :boolean(1)      default(TRUE)
#

class Assemblage < ActiveRecord::Base
  extend ActiveSupport::Memoizable
  belongs_to :piece
  serialize :block_list
  has_many :blocks, :dependent => :destroy
  has_many :tracks, :order => 'position', :dependent => :destroy
  has_many :block_undos, :dependent => :destroy
  has_many :block_redos, :dependent => :destroy
  has_many :scenes
  
  def report
    array = []
    transition_points.each do |tp|
      array << [tp,blocks_on_at(tp)]
    end
    array
  end
  def clear_undo_cue
    undos = BlockUndo.find_all_by_assemblage_id(self.id)
    undos.each do |undo|
      undo.destroy
    end
    blocks = Block.find_all_by_assemblage_id(self.id,:conditions => ['is_clone = ?', true])
    blocks.each do |block|
      Cueing.find_all_by_cuer_id(block.id).each{|x| x.destroy}
      Cueing.find_all_by_cuee_id(block.id).each{|x| x.destroy}
      block.destroy
    end
  end
  def transition_points
    points = non_cloned_blocks.inject([0,piece_duration]){|acc,x| acc << x.start_time ; acc << x.end_time+1}
    points.uniq.sort
  end
  memoize :transition_points

  def total_time_with(number)
    total = 0
    start_catch = nil
    stop_catch = nil
    number_of_blocks = blocks_at_transitions.length
    blocks_at_transitions.each_with_index do |tp,index|
      if !start_catch
        if tp[1].length == number
          start_catch = tp[0]
        end
      else
        if tp[1].length != number
          stop_catch = tp[0]
        end
      end
      if stop_catch
        this_catch = stop_catch - start_catch
        total += this_catch
        last_saved_start = start_catch
        start_catch = nil
        stop_catch = nil
      end
    end
    if start_catch
      stop_catch = blocks_at_transitions.last[0]
      this_catch = stop_catch - start_catch
      total += this_catch
    end
    # stop_catch = last_one[0]
    #     this_catch = stop_catch - start_catch
    #     total += this_catch
    percent = (total.to_f / piece_duration.to_f) * 100
    [total,sprintf("%.0f", percent)]
  end
  memoize :total_time_with
  def used_scenes #tested
    self.scenes.reject{|x| !x.start_time}
  end
  def non_cloned_blocks
    self.blocks.reject{|x| x.is_clone}
  end
  DISTRIBUTION = [1,2,2,3,3,3,3,2,2,1]
  def self.distributed_rand(low_limit,high_limit)
    delta = 1+high_limit - low_limit
    chunk_size = delta / DISTRIBUTION.length
    array = []
    (1..delta).each do |number|
      (1..DISTRIBUTION.length).each do |slice|
        if number <= chunk_size * slice
          DISTRIBUTION[slice-1].times{array << number-1 }
          break
          next
        end
      end
    end
    #array
    array.shuffle.pop + low_limit 
  end

  def blocks_on_at(time)
    non_cloned_blocks.select{|x| x.on_at?(time)}.sort_by{|x| x.title}
  end
  def tracks_on_at(time)
    blocks_on_at(time).map{|x| x.track}.uniq
  end
  def self.fill_track(track)
    diff =  Configuration.max_entrances - Configuration.min_entrances
    dance_number = Configuration.min_entrances + rand(diff+1)
    dance_number.times do
      block = Assemblage.make_block(track)
      track.blocks << block
    end
    track.enhance(Configuration.desired_on_time)
  end
  def fill_to_fit
    all_blocks = blocks.sort_by{|x| x.start_time}
    early_blocks = all_blocks.select{|x| x.start_time < piece_duration/2}
    late_blocks = all_blocks.select{|x| x.start_time >= piece_duration/2}.sort_by{|x| x.end_time}
    front_gap = all_blocks.first.start_time
    end_gap = piece_duration - late_blocks.last.end_time
    early_blocks.each do |block|
      logger.warn "******************moving forward #{block.title} #{block.id.to_s} --- #{front_gap.to_s}"
      block.start_time = block.start_time - front_gap
      block.save
    end
    late_blocks.each do |block|
      logger.warn "******************moving back #{block.title} #{block.id.to_s} --- #{end_gap.to_s}"
      block.start_time = block.start_time + end_gap
      block.save
    end
  end
  
  def self.make_block(track)
    #makes a block in the largest available space
    #its duration is within the limit
    space = track.largest_space
    gap = space[1]-space[0]
    max_duration = gap < Configuration.max_entrance_time ? gap : Configuration.max_entrance_time
    duration = Assemblage.distributed_rand(Configuration.min_entrance_time,max_duration)
    leftover_gap = (gap-duration)
    placement_offset = Assemblage.distributed_rand(0,leftover_gap)
    start_time = space[0]+placement_offset
    new_block = Block.new(
    :title => track.title,
    :start_time => start_time,
    :duration => duration,
    :cast => [],
    :assemblage_id => track.assemblage_id)
    new_block.save
    logger.warn "**************making #{new_block.title}"
    new_block
  end
  def generate
    total = 0
    tracks.each do |track|
      Assemblage.fill_track(track)
      # times = Assemblage.generate_block_times(piece_duration)
      # new_block = Block.new(
      # :title => 'generated',
      # :start_time => times[0],
      # :duration => times[1],
      # :cast => [],
      # :assemblage_id => id)
      # if track.can_accept_block?(new_block,maximum_on_time)
      #   new_block.save
      #   total += 1
      #   track.blocks << new_block 
      # end
      #track.consolidate_blocks
    end
    fill_to_fit
    re_fit_tracks
    total
    
  end
  def re_fit_tracks
    tracks.each do |track|
      track.refit
    end
  end
  def self.generate_random_duration(limit)
    longest = 600
    shortest = 60
    self.distributed_rand(shortest,longest)
  end
  def self.generate_random_start(piece_duration,block_duration)
    last_start = piece_duration - block_duration
    rand(last_start)
  end
  def self.generate_block_times(piece_duration)
    block_duration = Assemblage.generate_random_duration(piece_duration)
    start_time = Assemblage.generate_random_start(piece_duration,block_duration)
    [start_time,block_duration]
  end
  
  def blocks_at_transitions
    set = []
    transition_points.each do |tp|
      set << [tp,blocks_on_at(tp)]
    end
    set
  end
  memoize :blocks_at_transitions
  
  def enhance
    fill_to_fit
  end

  def perform_clone(user)
    new_assemblage = self.clone
    new_assemblage.created_by = user
    new_assemblage.title = 'clone of '+ self.title
    new_assemblage.save
    id_map = {}
    scene_hash = {}
    cue_array = []
    self.scenes.each do |old_scene|
      a_new = old_scene.clone
      a_new.assemblage_id = new_assemblage.id
      a_new.save
      scene_hash[old_scene.id] = a_new.id
    end
    self.tracks.each do |old_track|
      new_track = old_track.clone
      new_track.assemblage_id = new_assemblage.id
      new_track.save
      old_track.blocks.each do |old_block|
        unless old_block.is_clone
          new_block = old_block.clone
          new_block.assemblage_id = new_assemblage.id
          new_block.scene_id = scene_hash[old_block.scene_id]
          new_block.track_id = new_track.id
          new_block.save
          id_map[old_block.id] = new_block.id
          if old_block.cuer
            cue_array << old_block.id
          end
        end
      end
    end
    cue_array.each do |cuee|
      old_cuee = self.blocks.select{|x| x.id == cuee}.first
      old_cuer = old_cuee.cuer.id
      new_cuer = new_assemblage.blocks.select{|x| x.id == id_map[old_cuer]}.first
      new_cuee = new_assemblage.blocks.select{|x| x.id == id_map[cuee]}.first
      new_cuer.cuees << new_cuee
    end

    new_assemblage
  end
end

