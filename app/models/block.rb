# == Schema Information
#
# Table name: blocks
#
#  id            :integer(4)      not null, primary key
#  created_at    :datetime
#  created_by    :string(255)
#  event_id      :integer(4)
#  title         :string(255)
#  parent_id     :string(255)
#  updated_at    :datetime
#  start_time    :integer(4)
#  duration      :integer(4)
#  left          :integer(4)
#  div_class     :string(255)     default("e9f")
#  description   :string(255)
#  modified_by   :string(255)
#  grouped       :integer(4)      default(0)
#  blocklist_id  :integer(4)
#  assemblage_id :integer(4)
#  track_id      :integer(4)
#  cast          :text
#  z_index       :integer(4)      default(100)
#  cued_by_id    :integer(4)
#  is_clone      :boolean(1)      default(FALSE)
#  dependent     :boolean(1)      default(FALSE)
#  block_type    :string(255)
#  scene_id      :integer(4)
#

class Block < ActiveRecord::Base
  belongs_to :event
  belongs_to :assemblage
  belongs_to :track
  belongs_to :blocklist
  belongs_to :scene
  serialize :cast
  has_many :cues_to, :foreign_key => 'cuer_id', :class_name => 'Cueing'
  has_many :cues_from, :foreign_key => 'cuee_id', :class_name => 'Cueing'
  has_many :cuees, :through => :cues_to
  has_many :cuers, :through => :cues_from
  #belongs_to :cuer, :class_name => "Block", :foreign_key => 'cued_by_id'
  #has_many  :cuees, :class_name => "Block", :foreign_key => 'cued_by_id'
  named_scope :dependent, :conditions => ['dependent = ?', true]
  named_scope :is_grouped, lambda { |*args| {:conditions => ["grouped = ?", args.first]}}
  named_scope :in_assemblage, lambda { |*args| {:conditions => ["assemblage_id = ?", args.first]}}
  
  @@ati = []

  def move_by_difference(difference) #tested
    self.start_time = self.start_time + difference
  end

  def round_start_time(factor = 15) #tested
    self.start_time = (self.start_time.to_f/factor).round.to_i*factor
  end
  
  
  def cuer #tested
    cuers.reject{|x| x.is_clone}.last
  end
  def real_cuees #tested
    self.cuees(true).reject{|x| x.is_clone || x.cued_by_id == x.id || x.cuer.id != self.id}
  end
  def has_early_cuees? #tested
    self.cuees(true).select{|x| x.start_time < self.start_time}.length > 0
  end
  def has_late_cuer? #tested
    return false unless self.cuer
    self.cuer.start_time > self.start_time
  end
  def cuer_brothers #tested
    x = self.cuees
    x += self.cuers
    self.cuees.each do |cu|
      x += cu.cuers
    end
    x.uniq
  end
  
  def group_partners_from_array(array) #tested
    if self.grouped == 0
      list = array.select{|x| (x.id == self.id)}
    else
      list = array.select{|x| (x.grouped == self.grouped && x.assemblage_id == self.assemblage_id)}
    end
  end
  
  def overlapped(all_blocks, padding = 0) #tested
    overlapped_blocks = all_blocks.reject{|x| ((x.id == self.id) || (x.end_time+padding <= self.start_time) || (x.start_time-padding >= self.end_time))}
  end
  
  def castoverlapped(all_blocks,padding = 0)#tested
    all_blocks = all_blocks.reject{|x| x.is_clone }
    overlaps = []
    return overlaps if self.is_clone || self.new_record? || self.frozen?
    time_overlaps = self.overlapped(all_blocks,padding)
    if time_overlaps.length > 0
      time_overlaps.each do |block|
        next if block.id == self.id
        #check to see if casts have common members
        overlaps << block unless (block.cast & self.cast).empty?   
      end
    end
    #return array of overlapped blocks
    overlaps 
  end
  def reset_ati #tested
    @@ati = []
  end
  def set_duration(dur) #tested
    self.duration = dur
  end
  def expand(reference,factor)#tested
    difference = (((self.start_time - reference) * factor).to_i)
    self.start_time = reference + difference
  end

  def correct_swallowings #fixes the entire track a block is in saves it and returns the corected ones for updating on the page
    all_blocks = self.assemblage.blocks.select{|x| x.track_id == self.track_id}.reject{|x| x.is_clone}.sort_by{|x| x.start_time}
    corrected_blocks = Block.fix_z_indexes(all_blocks)
    all_blocks.each do |blo|
      blo.save
    end
    corrected_blocks
  end
  def change_z_index_if_needed(z_index) #tested
    return false if self.z_index == z_index
    self.z_index = z_index
    true
  end
  def self.fix_z_indexes(ordered_blocks_in_track) #tested
    changed = []
    ordered_blocks_in_track.each_index do |index|
      if index == 0
          changed << ordered_blocks_in_track[index] if ordered_blocks_in_track[index].change_z_index_if_needed(100)
      else
          if ordered_blocks_in_track[index].start_time <= ordered_blocks_in_track[index-1].end_time
            changed << ordered_blocks_in_track[index] if ordered_blocks_in_track[index].change_z_index_if_needed(ordered_blocks_in_track[index-1].z_index + 1)
          else
            changed << ordered_blocks_in_track[index] if ordered_blocks_in_track[index].change_z_index_if_needed(100)
          end
      end
      changed.uniq
    end
    
  end
  def self.add_block_from_params(params,assemblage_id,current_user)
    info = params[:id].split('-')
    no = info[1].to_i
    y_coord = info[2].to_i
    x_coord = info[3].to_i
    blocklist = Blocklist.find(no)
    block = Block.new
    block.get_attributes_from_blocklist(blocklist)
    block.created_by = current_user.login
    block.start_time = y_coord###
    block.track_id = x_coord
    block.assemblage_id = assemblage_id
    block.is_clone = true
    x = Scene.scene_id_for_block(block)    
    block.scene_id = x if x
    block.save
    @grouped_blocks = block.perform_all_actions_and_return_affected_blocks({:type => :single,:act => 'create',:push => :push}) do |x|
      x.make_undeleted
    end
  end

  def self.do_group_actions(some,params)
    case params[:task]
      when 'expand.js'
        first_block_time = some.first.start_time
        factor = params[:amount].to_f / 100
        grouped_blocks = some.first.perform_all_actions_and_return_affected_blocks({:type => :group,:act => 'update',:push => :push}) do |x|
          x.expand(first_block_time,factor)
        end
      when 'align.js'
        case params[:target]
          when 'first'
            target_time = some.first.start_time
          when 'last'
            target_time = some.last.start_time
        end
        grouped_blocks = some.first.perform_all_actions_and_return_affected_blocks({:type => :group,:act => 'update',:push => :push}) do |x|
          x.start_time = target_time
        end
      when 'round.js'
        grouped_blocks = some.first.perform_all_actions_and_return_affected_blocks({:type => :group,:act => 'update',:push => :push}) do |x|
          x.round_start_time(params[:target].to_i)
        end
    end#of case params[:task]
    grouped_blocks
  end
  def end_time #tested
    self.start_time + self.duration
  end
  
  def find_track(shift_amount) #not done
    oldtrack = Track.find(self.track_id)
    position = oldtrack.position.to_i + shift_amount.to_i
    new_track = Track.find_by_assemblage_id(oldtrack.assemblage_id,:conditions => "position = '#{position}'")
  end
  
  def convert_track_id(left) #tested
      @@ati << self.track_id
      if left == 0
        return @@ati
      end
      newtrack = self.find_track(left)
      if !self.assemblage.constrained
        if newtrack
          self.track_id = newtrack.id
          @@ati << newtrack.id
        end
      else
        if newtrack && newtrack.track_type == self.track.track_type
            self.track_id = newtrack.id
            @@ati << newtrack.id
        end
      end
      @@ati
  end
  
  
  def overlap_changes(before , after) #tested
    (before | after)-(before & after)
  end
  def involved_in?(number)
    
  end

  def check_for_overlaps? #tested
    @oc ||= self.assemblage.overlap_check
  end
  def overlaps?(other)
    return false if start_time > other.end_time
    return false if end_time < other.start_time
    true
  end

  def self.copyable_columns #tested
    columns.map{|x| x.name}.reject{|x| x == 'id'}
  end
  def undo_enabled?
    true
  end
  def make_deleted#tested
    self.is_clone = true
  end
  def make_undeleted#tested
    self.is_clone = false
  end
  def give_block_to_scene(id) #tested
    self.scene_id = id
  end
  def on_at?(time)
    time >= start_time && time <= end_time
  end
  
  def break_cueing #tested
    cue_er = Block.find(self.cuer.id)
    cueing = Cueing.find_by_cuer_id(self.cuer.id, :conditions => "cuee_id = '#{self.id}'")
    cueing.destroy
    cue_er
  end
  def get_attributes_from_blocklist(blocklist)
    self.cast = blocklist.cast
    self.title = blocklist.title
    self.event_id = blocklist.event_id
    self.duration = blocklist.duration ###
    self.div_class = blocklist.div_class
    self.dependent = blocklist.dependent
    self.blocklist_id = blocklist.id
  end
  def affected_track_ids
    @@ati.uniq
  end


  def get_array_of_overlapped_blocks(all_blocks,block_to_call)

    #first finds all cast_overlapped blocks
    #second moves the block
    #third finds new overlapped blocks
    #returns blocks which were changed by the move and need to be updated

    new_blocks = []
    if check_for_overlaps?
      block_before_overlaps = self.castoverlapped(all_blocks)
    end
      block_to_call.call(self)
      @@ati << self.track_id
    if check_for_overlaps?
      block_after_overlaps = self.castoverlapped(all_blocks)
      new_blocks += overlap_changes(block_before_overlaps,block_after_overlaps)
      @@ati += new_blocks.map{|x| x.track_id}
    end
    new_blocks
  end
  def get_cuer_from_array(all_blocks)
    result = []
    result += all_blocks.select{|x| x.id == self.cuer.id} if self.cuer
    #logger.info("*****cuer blocks*****"+result.map{|x| x.id}.join(','))
    result
  end
  def get_cuees_from_array(all_blocks)
    cuee_array = self.cuees.map{|x| x.id}
    all_blocks.select{ |x| cuee_array.include?(x.id)}
  end
  
  def get_grouped_blocks(all_blocks,type)
    case type
    when :group
      self.group_partners_from_array(all_blocks)
    when :scene
      all_blocks.select{|x| x.scene_id == self.scene_id}
    when :blocklist
      all_blocks.select{|x| (x.blocklist_id == self.blocklist_id && x.dependent)}
    else #single block
      x = all_blocks.detect{|x| x.id == self.id}
      [x]
    end
  end
  def track_cuers
    @track_cuers ||= self.assemblage.track_cues
  end
  def perform_all_actions_and_return_affected_blocks(opts = {} , &code_block)
    type = opts[:type] || :single
    action = opts[:act] || nil
    push = opts[:push] || nil
    blocks_to_change = []
    unswallowed_blocks = []
    cuer_bros = []
    if track_cuers
      all_blocks = Block.find_all_by_assemblage_id(self.assemblage_id,:include => [:cuers,:cuees]).reject{|x| (x.is_clone && x.id != self.id)}
    else
      all_blocks = Block.find_all_by_assemblage_id(self.assemblage_id).reject{|x| (x.is_clone && x.id != self.id)}
    end
    #all_blocks = self.assemblage.blocks.reject{|x| (x.is_clone && x.id != self.id)}
    grouped_blocks = self.get_grouped_blocks(all_blocks,type)
    #logger.info("*****grouped blocks*****"+grouped_blocks.map{|x| x.id}.join(','))
    return nil if grouped_blocks.length < 1

    grouped_blocks.each do |block|
      blocks_to_change += block.get_array_of_overlapped_blocks(all_blocks,code_block)

      if track_cuers
        cuer_bros += block.get_cuer_from_array(all_blocks)
        cuer_bros += get_cuees_from_array(all_blocks)
      end
      #logger.info("*****changed blocks*****"+blocks_to_change.map{|x| x.id}.join(','))
    end
    
    # now go through the affected tracks and clean up the z-indexes and return with some new blocks to update
    #logger.info("*****tracks******"+affected_track_ids.join(','))
    affected_track_ids.each do |track_id|
      trackmates = all_blocks.select{|x| (x.track_id == track_id) && !x.is_clone}.sort_by{|x| x.start_time}
      unswallowed_blocks += Block.fix_z_indexes(trackmates)
      #logger.info("*****unswallowed blocks*****"+unswallowed_blocks.map{|x| x.id}.join(','))
    end
    
    #add the unswallowed blocks to the blocks changed by the main action and uniq them
    all_changed_blocks = blocks_to_change + unswallowed_blocks + grouped_blocks
    #save all the changed blocks
    #logger.info("*****all changed blocks not uniq*****"+all_changed_blocks.map{|x| x.id.to_s + ' - '+ x.start_time.to_s}.join(','))
    all_changed_blocks = all_changed_blocks.uniq
    # set the undo here using the dirty object functionality
    if push && undo_enabled?
      new_block = action == 'create'? self.id : nil
      Block.create_undo(all_changed_blocks,action,new_block)
    end
    #logger.info("*****all changed blocks*****"+all_changed_blocks.map{|x| x.id.to_s + ' - '+ x.start_time.to_s}.join(','))
    all_changed_blocks.each{|x| x.save}
    # return the array of changed blocks to update on the page
    all_changed_blocks + cuer_bros
  end
  def cast_list
    cast.join(' ')
  end
  def self.create_undo(grouped_blocks,edit_type,new_block)
      clone_data = []
      b = BlockUndo.new
      count = grouped_blocks[0].assemblage.block_undos.count
      blocks = []

        grouped_blocks.each do |block|
          updatable = {}
          Block.copyable_columns.each do |cc|
            if eval "block.#{cc}_changed?"
              updatable[cc] = eval "block.#{cc}_was"
            end
          end
          clone_data << {'id' => block.id, 'data' => updatable}
          blocks << block.id if new_block && new_block == block.id
        end
        
      b.clone_data = clone_data
      b.blocks = blocks
      b.position = count + 1
      b.edit_type = edit_type
      grouped_blocks[0].assemblage.block_undos << b
  end

end

