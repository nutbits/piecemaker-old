class Video < ActiveRecord::Base
  
  MOVIE_EXTENSIONS = %w[mov mp4 m4v flv]
  
  #belongs_to :event
  has_many :events,:dependent => :nullify, :order => :happened_at,:conditions => "state = 'normal'"
  has_many :video_recordings, :dependent => :destroy
  has_many :subjects, :through => :video_recordings, :source => :piece, :uniq => true
  
  named_scope :in_archive, :conditions => "fn_arch is not null"
  named_scope :active, :conditions => "state = 'normal'"

if Configuration.app_is_local?  
  require 'video_definitions'
  extend VideoDefinitions
  require 'video_qt_player'
  extend QtPlayer
  require 'video_file_manipulation'
  extend FileManipulation
  include FileInstanceMethods
end
  
  after_save :rename_files_on_title_change###################


  def rename_files_on_title_change###################
    if self.title_changed?
     logger.info '******* title changed moving files'
   end
  end
  def update_from_params(params)
    #self.title = params[:title].split('.').first
    self.fn_s3 = '.' + params[:title].split('.').last
    save
  end
  def source_string
    sources = [] 
    if fn_local
      sources << fn_local
    else
      sources << ''
    end
    if fn_s3
      sources << fn_s3
    else
      sources << ''
    end
    if fn_arch
      sources << fn_arch
    else
      sources << ''
    end
    sources.join(',')
  end
  def mark_in(place)
    logger.info '******* trying to mark'
    marker = '.mp4'
    case place
    when 'local'
      self.fn_local = marker
      save
    when 's3'
      self.fn_s3    = marker
      save
    when 'archive'
      self.fn_arch  = marker
      save
    end
  end
  def mark_not_in(place)
    marker = nil
    case place
    when 'local'
      self.fn_local = marker
      save
    when 's3'
      self.fn_s3    = marker
      save
    when 'archive'
      self.fn_arch  = marker
      save
    end
  end
  def full_s3_path
    x = self.s3_path.split('.')
    x[1] = x[1] == 'flv' ? 'flv' : 'mp4'
    "#{x[1]}:#{x[0]}"
  end

  def self.split_ext(file_name)
    file_name.split('.').last
  end
  def give_to_piece(remove_piece,new_piece)
    new_piece.recordings << self
    subjects.delete(remove_piece)
    subjects << new_piece
  end
  
  def base_name
    return false unless title
    title.split('.').first
  end
  def date_prefix
    return false unless base_name
    split = base_name.split('_').first
    return false unless split && split =~ /\d\d\d\d\d\d/
    split
  end
  def date_serial_number
    return false unless base_name
    split = base_name.split('_').second
    return false unless split && split =~ /\d\d\d/
    split
  end
  def title_string
    return false unless base_name
    split = base_name.split('_').third
    return false unless split && split.length > 1
    split
  end
  def uses_conventional_title?
    date_prefix && date_serial_number && title_string
  end
  def give_date_from_title
    if uses_conventional_title?
      pref = date_prefix[0..3] + '-' + date_prefix[4..5] + '-' + date_prefix[6..7]
      begin
        date = Date.parse(pref)
        date = date + 12.hours
        date = date + date_serial_number.to_i.hours
        self.recorded_at = date
      rescue
      end
    end
  end
  def serial_number
    (piece.recordings.index(self)+1).to_s
  end
  def comes_before(video2)
    return false unless date_prefix && video2.date_prefix
    return false if video2.date_prefix.to_i < date_prefix.to_i
    return true if video2.date_prefix.to_i > date_prefix.to_i
    return false unless date_serial_number && video2.date_serial_number
    return false if video2.date_serial_number.to_i <= date_serial_number.to_i
    true
  end
  def guess_recording_date
    first_part = title.split('_').first
    if first_part =~ /\d\d\d\d\d\d\d\d/
      first_part
    else
      "???"
    end
  end
  def self.parse_date_from_title(title)
      base_title = title.split('.').first
      date_part = base_title.split('_').first
      year = date_part[0..3]
      month = date_part[4..5]
      day = date_part[6..7]
      mdy = month+'/'+day+'/'+year
      mdy.to_date
  end
  def times
    tims = events.map{|x| [x.video_start_time,x.duration,x.id]}
    tims.sort_by{|x| x[0]}
    times = tims.to_json
  end
  def event_at(time)
    events.select{|x| x.happened_at - recorded_at < time}.last
  end
  def set_new_title(piece)
    
    time = Time.now.strftime("%Y%m%d")
      last_dvd = piece.recordings.last
      if last_dvd && last_dvd.title
        last_title = last_dvd.title.split('_')
        last_time = last_title[0]
        last_number = last_title[1].to_i
        if last_time == time #same day #increment serial number
          new_number = last_number + 1
        else #new day number = 1
          new_number = 1
        end
        
      else #no dvd make a new title
        new_number = 1
      end
      new_title = time + '_' + Event.pad_number(new_number)
      new_title << '_' + piece.short_name
      self.title = new_title
    
  end
  def self.make_segment(from,to,start_time,duration)
    RAILS_ROOT + "/vendor/bin/#{Configuration.arch_type}/HEAD/bin/ffmpeg -i #{from} -ss #{start_time} -t #{duration} -vcodec copy -y #{to}"
  end
  def make_clip(start_time,duration,prefix = 'clip')
    finished_path = Video.uncompressed_dir + '/'+ prefix + '-' + title + '-' + start_time.to_i.to_s + '-' + duration.to_i.to_s + '.mp4'
    command = Video.make_segment(full_uncompressed_path,finished_path,start_time,duration)
    system command
  end

  def show_existing_versions
    string = ''
    string << "Archive: #{fn_arch}" if fn_arch
    string << ', ' if string.length > 0 && fn_s3
    string << "S3: #{fn_s3}" if fn_s3
    string << ', ' if string.length > 0 && fn_local
    string << "Local: #{fn_local}" if fn_local
    string
  end
  def show_subjects
    string = ''
    self.video_recordings.each do |subject|
      string << '*' if subject.primary
      string << subject.piece.title
      string << '<br />'
    end
    string
  end
  def guess_piece_title
    name = title.split('.')
    name.pop
    name      = name.join('.')
    last_part = name.split('_').last

  end
  def false_string
    '<span style = "color:#a00">F</span>'
  end
  def true_string
    '<span style = "color:#0a0">T</span>'
  end
  def local_show
    fn_local ? true_string : false_string
  end
  def archive_show
    fn_arch ? true_string : false_string
  end
  def S3_show
    fn_s3 ? true_string : false_string
  end
  def self.list_in_s3
    if @sl == nil
      begin
        logger.warn{'********** looking in s3'}
        @sl = S3Config.connect_and_get_list
      rescue
        @sl = false
      end
    end
    @sl
  end
  def confirm_presence(locats = ['uncompressed'])
    result = {:message => '',:error => '' }
    if locats.include? 'uncompressed'
      if local_present?
        self.fn_local = '.mp4'
      else
        self.fn_local = nil
      end
    end
    if locats.include? 'archive'
      case archive_present?
      when 'true'
        self.fn_arch = '.mp4'
      when 'false'
        self.fn_arch = nil
      else
        result[:error] << 'No Archive Access'
      end
    end
    if locats.include? 's3'
      case s3_present?
      when 'true'
        self.fn_s3 = '.mp4'
      when 'false'
        self.fn_s3 = nil
      else
        result[:error] << ' No S3 Access'
      end
    end
     save
     result
  end
  def viewable?
    (!Configuration.app_is_local? && fn_s3) || ( Configuration.app_is_local? && (fn_arch || fn_local || fn_s3))
  end
  def meta_data_present
    meta_data ? 'True' : 'False'
  end
  def self.update_heroku
    if Configuration.app_is_local?
      Dir.chdir(RAILS_ROOT)
      system "heroku db:push --app piecemaker-#{Configuration.s3_base_folder} --confirm piecemaker-#{Configuration.s3_base_folder}"
    end
  end
  def self.fix_titles(from, to)
    vids = all.select{|x| x.title =~ /#{Regexp.escape(from)}/}
    puts "fixing #{vids.length} files"
    vids.each do |vid| 
      new_title = vid.title.gsub(from,to)
      puts "renaming #{vid.title}"
      rename_file_locations(vid.title,new_title)
      rename_s3(vid.title,new_title)
    end
  end

end


# == Schema Information
#
# Table name: videos
#
#  id          :integer(4)      not null, primary key
#  title       :string(255)
#  recorded_at :datetime
#  duration    :integer(4)
#  fn_local    :string(255)
#  fn_arch     :string(255)
#  fn_s3       :string(255)
#  vid_type    :string(255)     default("rehearsal")
#  rating      :integer(4)      default(0)
#  meta_data   :text
#  created_at  :datetime
#  updated_at  :datetime
#

