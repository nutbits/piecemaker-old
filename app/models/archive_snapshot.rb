class ArchiveSnapshot < ActiveRecord::Base
  serialize :snapshot
  def self.diff_snapshots(snap1,snap2)
   ArchiveSnapshot.diff_lists(snap1.snapshot,snap2.snapshot)
  end
  def self.diff_lists(list1,list2)
    list1 ||= []
    list2 ||= []
     {:missing_from_first => list2 - list1,
        :missing_from_second => list1 - list2}
  end
  # def snapshot
  #   self[:snapshot] ? Marshal.load(self[:snapshot]) : nil
  # end
  # 
  # def snapshot=(x)
  #   self[:snapshot] = Marshal.dump(x)
  # end
end

# == Schema Information
#
# Table name: archive_snapshots
#
#  id         :integer(4)      not null, primary key
#  snapshot   :text
#  created_at :datetime
#  updated_at :datetime
#

