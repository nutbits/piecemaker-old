# == Schema Information
#
# Table name: block_undos
#
#  id            :integer(4)      not null, primary key
#  blocks        :text
#  position      :integer(4)
#  assemblage_id :integer(4)
#  edit_type     :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  clone_data    :text
#

class BlockUndo < ActiveRecord::Base
  belongs_to :assemblage
  serialize :blocks
  serialize :clone_data
  
  def perform_undo
      grouped_blocks = []
      erase = nil
      clone_data.each do |undo_block|
        original = Block.find_by_id(undo_block['id'].to_i)
        if original
          undo_block['data'].each do |key,datum|
            datum = "'#{datum}'" if datum.class == String
            datum = "['" + datum.join("','") + "']" if datum.class == Array
            datum = 'nil' unless datum
            datum = 'false' if key == 'is_clone'
            eval "original.#{key} = #{datum}"
          end
          original.save
          grouped_blocks << original
         
          if edit_type == 'create' && blocks[0] && blocks[0] == original.id
            Cueing.find_all_by_cuer_id(original.id).each{|x| x.destroy}
            Cueing.find_all_by_cuee_id(original.id).each{|x| x.destroy}
            original.destroy
            erase = original.id
          end
        end
      end
        grouped_blocks.each do |block|
          grouped_blocks += block.cuer_brothers
        end
      grouped_blocks = grouped_blocks.uniq
      [grouped_blocks,erase]
  end
end

