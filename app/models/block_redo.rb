# == Schema Information
#
# Table name: block_redos
#
#  id            :integer(4)      not null, primary key
#  blocks        :text
#  position      :integer(4)
#  assemblage_id :integer(4)
#  edit_type     :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class BlockRedo < ActiveRecord::Base
  belongs_to :assemblage
  serialize :blocks
end
