# == Schema Information
#
# Table name: blocklists
#
#  id         :integer(4)      not null, primary key
#  created_at :datetime
#  created_by :string(255)
#  event_id   :string(255)
#  title      :string(255)
#  duration   :integer(4)      default(120)
#  div_class  :string(255)     default("e9f")
#  left       :integer(4)      default(400)
#  used       :integer(4)      default(0)
#  piece_id   :integer(4)
#  cast       :text
#  dependent  :boolean(1)      default(FALSE)
#  event_type :string(255)     default("dance")
#

class Blocklist < ActiveRecord::Base
  belongs_to :piece
  has_many :blocks
  serialize :cast
  
  before_destroy :clean_up_dependents
  
  def has_blocks_in_assemblage?(id)#tested
     x = self.blocks.select{|x| x.assemblage_id == id && !x.is_clone}
    x.length > 0
  end
  def self.create_from_events(events)#tested
    array = []
    events.each do |event|
      array << self.create(
      :title => event.title,
      :piece_id => event.piece_id,
      :event_id => event.id,
      :cast => event.performers || []
      )
    end
    array
  end
  def clean_up_dependents
    self.blocks.each do |block|
      block.dependent = false
      block.save
    end
  end
  def permanently_unused? #tested
    [' dance_scene',' music_note',' light_cue',' bill_cue'].include?(self.title)
  end
  def update_dependent_blocks #tested
    self.blocks.each do |block|
      if block.dependent
        block.title = self.title
        block.cast = self.cast
        block.duration = self.duration            
        block.div_class = self.div_class
        block.save
        block.correct_swallowings
      end
    end
  end
end

