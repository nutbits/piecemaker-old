# == Schema Information
#
# Table name: tracks
#
#  id            :integer(4)      not null, primary key
#  title         :string(255)
#  assemblage_id :integer(4)
#  created_at    :datetime
#  updated_at    :datetime
#  color         :string(255)     default("99c")
#  position      :integer(4)
#  track_type    :string(255)
#

class Track < ActiveRecord::Base
  belongs_to :assemblage
  acts_as_list :scope => :assemblage_id
  has_many :blocks
  
  
  def non_cloned_blocks
    self.blocks.reject{|x| x.is_clone}
  end
  def largest_space
    winner_size = 0
    winner = nil
    spaces.each do |space|
      length = space[1]-space[0]
      if length > winner_size
        winner = space
        winner_size = length
      end
    end
    winner
  end
  def fill_to_fit
    all_blocks = blocks.sort_by{|x| x.start_time}
    early_blocks = all_blocks.select{|x| x.start_time < assemblage.piece_duration/2}
    late_blocks = all_blocks.select{|x| x.start_time >= assemblage.piece_duration/2}.sort_by{|x| x.end_time}
    front_gap = all_blocks.first.start_time
    end_gap = assemblage.piece_duration - late_blocks.last.end_time
    early_blocks.each do |block|
      block.start_time = block.start_time - front_gap
      block.save
    end
    late_blocks.each do |block|
      block.start_time = block.start_time + end_gap
      block.save
    end
  end
  def spaces
    length = assemblage.piece_duration
    return [[0,length]] if blocks.length == 0
     spaces = []
     new_start = 0
    
    blocks.sort_by{|x| x.start_time}.each do |block|
         spaces << [new_start,block.start_time]
         new_start = block.end_time + 1
       end
     spaces << [new_start,length]
    spaces
  end
  def refit
    diff = Configuration.desired_on_time - total_time
    correct = diff/blocks.length
    blocks.each do |block|
      block.duration = block.duration + correct
      block.save
    end
  end
  def enhance(desired)
    return if blocks.length == 0
    diff = desired - total_time
    shuffled_blocks = blocks.shuffle
    shuffled_blocks.each do |block|
      next if block.duration + diff > Configuration.max_entrance_time
      next if block.duration + diff < Configuration.min_entrance_time
      block.duration = block.duration + diff
      block.save
      break
    end
    
  end
  def old_enhance(desired)
    return if blocks.length == 0
    diff = desired - total_time
    useful_blocks = blocks
    useful_blocks.each do |block|
      if block.duration < Configuration.min_entrance_time + diff/useful_blocks.length
        useful_blocks.delete(block)
      end
    end
    leftover = 0
    addon = diff/blocks.length
    blocks.sort_by{|x| x.start_time}.each_index do |index|
      if addon > 0
        overflow = blocks[index].duration + addon - Configuration.max_entrance_time
        if overflow > 0
          addon = addon - overflow
          leftover = leftover + overflow
        end
        blocks[index].duration = blocks[index].duration + addon
        addon = addon + leftover
        leftover = 0
      else
        overflow = blocks[index].duration + addon - Configuration.min_entrance_time
        if overflow < 0
          addon = addon - overflow
          leftover = leftover + overflow
        end
        blocks[index].duration = blocks[index].duration + addon
        addon = addon + leftover
        leftover = 0
      end
      
        # if blocks[index+1]
        #           blocks[index].start_time = blocks[index].start_time - Track.overlap_amount(blocks[index],blocks[index+1])
        #         end
          blocks[index].save
      end

  end
  def de_overlap
    blocks.sort_by{|x| x.start_time}.each do |block|
      
    end
  end
  def self.overlap_amount(b1,b2)
    ov = b2.start_time - b1.end_time
    return 0 if ov > 0
    ov * -1
  end
  
  
  def total_time
    non_cloned_blocks.inject(0){|sum,x| sum + x.duration}
  end
  def can_accept_block?(new_block,maximum_on_time)
    return false if total_time + new_block.duration > maximum_on_time
    non_cloned_blocks.each do |ncb|
      return false if ncb.overlaps?(new_block)
    end
    true
  end
  def consolidate_blocks(threshold = 10)
    ordered = blocks.sort_by{|x| x.start_time}
    first_block = ordered.shift
    if first_block
      ordered.each do |one_block|
        separation = one_block.start_time - first_block.end_time
        if separation < threshold
          first_block.duration = one_block.duration + first_block.duration + separation
          one_block.destroy
          first_block.save
        else
          first_block = one_block
        end
        
      end
    end
  end
end

