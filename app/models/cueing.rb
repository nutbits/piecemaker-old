# == Schema Information
#
# Table name: cueings
#
#  id         :integer(4)      not null, primary key
#  cuer_id    :integer(4)
#  cuee_id    :integer(4)
#  trigger    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Cueing < ActiveRecord::Base
  belongs_to :cuer, :class_name => 'Block', :foreign_key => 'cuer_id'
  belongs_to :cuee, :class_name => 'Block', :foreign_key => 'cuee_id'
end

