module BlocksHelper
  def block_needs_cast?
    if @block.assemblage.constrained
      BLOCKS_WITH_CAST.include?(@block.track.track_type)
    else
      true
    end
  end
  def blocklists_for_select
    bl = ['none']
    if @block.assemblage.constrained
      bl += @blocklists.select{|x| x.event_type == @block.track.track_type}.map{|x| [x.title,x.id]}
    else
      bl += @blocklists.map{|x| [x.title,x.id]}
    end
    logger.info { options_for_select(bl) }
    options_for_select(bl)
  end
end