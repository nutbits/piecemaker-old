module AssemblagesHelper
  
  def piece_start_time
    @pstt ||= @assemblage.start_time#(15*60*60) #this is an absolute clock time for use when neccessary
  end

  def top_offset
    120
  end
  def start_offset
    75 #how much the whole thing is shifted to the right because of track labels
  end
  def block_shift
     pre_roll + offset_pix
  end

  def overlap_shrink_factor
    7 
  end
  def offset_pix
    0
  end

  def div_id(block)
    'block-'+block.id.to_s
  end
  def tip_id(block)
    'tip-'+block.id.to_s
  end
  def block_height(block)
     bh = @track_height - (block.z_index-99)*overlap_shrink_factor + 2
     unless castover(block).empty?
       bh -= 2
     end
     bh
  end
  def castover(block)
    @castover ||= {}
    @castover[block.id] ||= @assemblage.overlap_check ?  block.castoverlapped(@blocks): []
  end
  def bord(block)
    bord = (@assemblage.track_cues && ( !castover(block).empty? || block.has_early_cuees? || block.has_late_cuer?)) ? ' bord': ''
  end
  def cue(block)
    cue = block.cued_by_id ? ' cued' : ''
  end
  def depend(block)
    depend = block.dependent ?  'depend' : ''
  end
  
  def duration_pixels(block)
    scale_out(@time_scale,block.duration)
  end
  def start_pixels(block)
    scale_out(@time_scale,(block.start_time+pre_roll))+start_offset
  end
  def show_ancestors(block)
    if block.event_id
      "<a class='salk get'href='/assemblages/show_ancestors/#{block.event_id}'>Show Original</a>&nbsp;"
    else
      ''
    end
  end
  def tooltipize(block,castover)
    block.cast ||= []
    group_label = block.grouped == 0 ? 'Select' : 'Unselect'
    
    text = ''
    text = "<div id='tip-close'></div><br />"
    text << '<div class="tip-link">'
    text << '<a class="delk"href="#">Delete</a>&nbsp;'
    text << '<a class="grlk"href="#">'+group_label+'</a>&nbsp;'
    text << "<a class='bllk'href='/blocklists/create_from_block/#{block.id}'>Add to Block List</a>&nbsp;"
    text << show_ancestors(block)
    text << '</div><br />'
    text << block.title + ' id: ' + block.id.to_s + '<br />'
    text << '<br />Start: '
    text << minutify((block.start_time),true,false)
    text << '<br />Duration: '
    text << minutify(block.duration,true,false)
    text << '<br />'
    
    if block.cast.length > 0
      text << ("Cast: " + block.cast.join(' ') )
    end
    if (block.description && block.description.length > 0)
      text << '<br /><br />'
      text << ("Description: <br />" + block.description)
    end
    text << '<br /><br />'
    if @assemblage.track_cues
      unless block.real_cuees.empty?
        text << 'Cues: '
        text << cuee_list(block)
        text << '<br />'
      end
      if block.cuer && block.cuer.id != block.id
        text << "Cued by: <a class='brcu'title='Remove this cue relationship.'href='/assemblages/break_cueing/#{block.id}'>"
        text << block.cuer.title
        text << '</a><br />'
      end
    end
    unless castover.empty?
      text << '<br /><span style = "color:#f00">Cast Conflict With: '
      text << castover.map{|x| x.title}.join(', ')
      text << '</span><br />'
    end
    if @assemblage.track_cues
      text << '<br />'
      text << cuee_warning(block)
      text << '<br />'
      text << '<br />'
      text << cuer_warning(block)
      text << '<br />'
    end
    text
  end

  def cuee_list(block)
    block.real_cuees.map{|x| x.title}.join(', ')
  end
  def cuee_warning(block)
    bs = block.real_cuees.select{|x| x.start_time < block.start_time}
    if bs.length > 0
      text = "<span style = 'color:#f00'>This block is behind it's cuee #{bs.map{|x| x.title}.join(', ')}</span>"
    else
      ''
    end
  end
  def cuer_warning(block)
    if block.cuer && block.cuer.start_time > block.start_time
      text = "<span style = 'color:#f00'>This Block is Before it's Cuer: #{block.cuer.title} </span>"
    else
      ''
    end
  end
  def put_undo(assemblage)
    unless assemblage.block_undos.empty?
      text = link_to "Undo", :action => 'undo_one', :id => @assemblage.id 
    end
  end

  def scale_out(time_scale,x)
    out = (time_scale > 1)? x/time_scale :  x*time_scale.abs
  end
  def scale_in(time_scale,x)
    inbase = (time_scale > 1)? x*time_scale :  x/time_scale.abs
  end  
  
  def ratio(x)
    (x/4).to_i
  end
  def minutify(time_in_seconds, seconds = false, hours = false)    
    prefix = time_in_seconds < 0 ? '-': ''
    time_in_seconds = time_in_seconds.abs
    timestring = ''
     hour_string = ''
     minute_string = ''
     second_string = ''
    hourmin = time_in_seconds.divmod(3600)
    minsec = hourmin[1].divmod(60)
        if(hourmin[0] < 10)
          hour_string << '0'
        end
      hour_string << hourmin[0].to_s+':'
      if(minsec[0] < 10)
        minute_string << '0'
      end
    minute_string << minsec[0].to_s
    second_string << ':'
    if(minsec[1] < 10)
      second_string << '0'
    end
    second_string << minsec[1].to_s
    timestring << prefix
    timestring << hour_string if hours
    timestring << minute_string
    timestring << second_string if seconds
    timestring
  end
  
  def minutify_old(time_in_seconds)
    hourmin = time_in_seconds.divmod(3600)
    minsec = hourmin[1].divmod(60)
    timestring = ''
    if(hourmin[0] > 0)
        if(hourmin[0] < 10)
          timestring << '0'
        end
      timestring << hourmin[0].to_s+':'
    end
      if(minsec[0] < 10)
        timestring << '0'
      end
    timestring << minsec[0].to_s
    timestring << ':'
    if(minsec[1] < 10)
      timestring << '0'
    end
    timestring << minsec[1].to_s
  end
 
  def time_listing_track_title(block)
    block.track.title
  end
  def parse_time(sections)
    minsec = sections[0].split(':')
    time = minsec[0].to_i*60 + minsec[1].to_i
  end
  def put_inlet(block)
    if @assemblage.track_cues && block.cuer && !block.cuer.is_clone
      "<div class = 'inlet' style = 'background:##{background_color(block.cuer)}'></div>"
    end
  end

  def put_outlet(block)
    if @assemblage.track_cues && (block.cuees.reject{|x| x.is_clone}.length > 0 || block.cued_by_id == 0)
      if block.cuees.select{|x| x.cuer.id == block.id}.length > 0
        ex = ' red' if block.has_early_cuees?
        ex = ' white' if block.cued_by_id == 0
        "<div class = 'outlet#{ex}'></div>"
      end
    end
  end

  def junk_convert(number)
    (number/100) * 85
  end
  
  def display_style
    @display_style ||= 'absolute'
  end
  def start_time
    @st ||= piece_start_time - pre_roll #absolute clock time
  end


  def pre_stripes            
    @ps ||= pre_roll / stripe_seconds
  end
  def post_stripes
    @pst ||= (pre_roll+piece_duration) / stripe_seconds
  end

  def stripe_pixel_width
    @spw ||= stripe_seconds * @time_scale
  end
  
  def hidden_count
    0
  end
  
   def grid_time_display(i)
     #this is fucked up for now to help with biarteca
     minutify(i * stripe_seconds + start_time + 21*60*60, false, @hours)
   end
  
   def grid_border_thickness
     case GRID_RHYTHM[@time_scale]
                 when 4 then cycle(' b3',' b1',' b1',' b1', :name => 'thickness')
                 when 5 then cycle(' b3',' b1',' b1',' b1',' b1', :name => 'thickness')
                 when 2 then cycle(' b3',' b1', :name => 'thickness')
                 when 3 then cycle(' b3',' b1',' b1', :name => 'thickness')
                 else cycle(' b3',' b1',' b1',' b1',' b1',' b2',' b1',' b1',' b1',' b1', :name => 'thickness')
                 end
   end
  def grid_border_color(i)
    border_color = ''
    if (i == pre_stripes || i == post_stripes)
      border_color = ' rdd'
      reset_cycle('thickness')
    end
    border_color
  end
  def put_title(block)
    text = block.title
    if block.scene
      text << '<br />'
      text << "<span style = 'font-weight:normal'>(#{block.scene.title})</span>"
    end
    text
  end

  def scene_number(scene)
    scenenum = @scenes.reject{|x| !x.start_time}.sort_by{|x| x.start_time}.index(scene)
  end
  def track_background_color(track)
    if @assemblage.constrained
      CLASS_COLORS[track.track_type.to_sym]
    else
      track.color
    end
  end
  def background_color(block)
    if @assemblage.constrained
      CLASS_COLORS[block.track.track_type.to_sym]
    else
      block.div_class
    end
  end
  def extra_class(block)
    @assemblage.constrained ? block.track.track_type : ''
  end
  def cue_message(block)
    mess = ''
    mess = '<span class = "cue-message">&nbsp;&nbsp;[cued by: '+block.cuer.title+']</span>' if block.cuer
    mess
  end
  def listing_title(block)
    string = ''
    string << '** CUE: ' if block.track.track_type == 'cues'
    string << block.title
    string << cue_message(block)
    string
  end
  def show_cast_in_listing?(block)
    block.cast.length > 0 && ( !@assemblage.constrained || block.track.track_type == 'dance' )
  end
  def show_block_description_in_listing?(block)
    true
  end
  def listing_description(block)
    text = ''
    if show_cast_in_listing?(block)
      text << '<span style = "font-style:italic">'
      text << block.cast.join(' ')
      text << '</span><br/>'
    end
    if show_block_description_in_listing?(block)
      text << block.description if block.description
    end
    text
    
  end
end
