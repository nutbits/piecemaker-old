less_stripes = number_of_stripes
less_stripes -= 1
start_offest = 150
track_height = 35
full_height = (@assemblage.tracks.length+1) * track_height
pdf.font 'Helvetica', :size => 14, :weight => 'bold'


#track and label
@assemblage.tracks.reverse.each_with_index do |track,index|
  draw_height = track_height*index
  pdf.fill_color cycle("ffffff","e0e0e0")
  pdf.fill_polygon [0,draw_height], 
  [(less_stripes+1)*stripe_width+start_offset+25,draw_height],
  [(less_stripes+1)*stripe_width+start_offset+25,draw_height+track_height],
  [0,draw_height+track_height]
  pdf.fill_color '000000'
  pdf.draw_text track.title, :at => [2,track_height*index+3]
  #pdf.stroke_line([0,draw_height], [less_stripes*stripe_width+start_offset, draw_height])

end
pdf.font 'Courier', :size => 10

for i in 0..less_stripes
   pdf.bounding_box [start_offest+stripe_width*i, full_height], :width => stripe_width-2, :height => 20 do
     
     pdf.draw_text grid_time_display(i), :at => [-34,10]
   end
   if ['00'].include? grid_time_display(i)[-2,2]
     pdf.line_width 3
   elsif ['15','30','45'].include? grid_time_display(i)[-2,2]
     pdf.line_width 2
   else
     pdf.line_width 1
   end
 pdf.stroke_line([start_offest+stripe_width*i,full_height],[start_offest+stripe_width*i,0])
end
pdf.font 'Helvetica', :size => 11
@assemblage.tracks.reverse.each_with_index do |track,index|
  draw_height = track_height*index
  start_offset = 75
  track.non_cloned_blocks.each do |block|
    
    start_position = start_pixels(block)+start_offset
    pdf.fill_color "b0ddc0"
    pdf.fill_polygon [start_position,draw_height+1], 
    [start_position+duration_pixels(block),draw_height+1],
    [start_position+duration_pixels(block),draw_height+34],
    [start_position,draw_height+34]
    pdf.stroke_color "709980"
    pdf.line_width 2
    pdf.stroke_line [start_position+2,draw_height+1],[start_position+2,draw_height+34]
    pdf.fill_color "000000"
    
    pdf.draw_text put_title(block), :at => [start_position+6,draw_height+24]
  end
end