class BlocksController < ApplicationController
  layout  "standard"
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    redirect_non_admins('create_events',home_url) and return
    @blocks = Block.paginate(
                    :per_page => 50,
                    :page => params[:page],
                    :order => sort_from_universal_table_params)
  end

  def show
    redirect_non_admins('create_events',home_url) and return
    @block = Block.find(params[:id])
  end

  def new
    @block = Block.new    
  end


  def edit
    @block = Block.find(params[:id])
    respond_to do |format|
      format.html {}
      format.js {render :layout => false} 
    end

  end

  def edit_from_click
      as = Assemblage.find(session[:assemblage_id])
      info = params[:id].split('-')
      start_time = info[0].to_i
      track_id = info[1].to_i
      div_class = as.constrained ? CLASS_COLORS[Track.find(track_id).track_type.to_sym] : Track.find(track_id).color
      
      @block = Block.new(
        :div_class     => div_class,
        :track_id      => track_id,
        :duration      => 120,
        :start_time    => start_time,
        :cast          => [],
        :assemblage_id => as.id,
        :created_by    => current_user.login,
        :title         => '',
        :description   => ''
      )
      x = Scene.scene_id_for_block(@block)    
      @block.scene_id = x if x
      @block.save
      @create = true
      @double_click = true      
      @blocklists = Blocklist.find_all_by_piece_id(as.piece_id)
      respond_to do |format|
        format.html {}
        format.js {render :action => 'edit', :layout => false} 
      end
      
  end

  def cancel_add_from_click
    
    block = Block.find(params[:id])
    block.destroy
    render :nothing => true
  end
  
  def destroy
    block = Block.find(params[:id])
    block.destroy
    redirect_to :action => 'list'
  end

end
