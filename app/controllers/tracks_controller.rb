class TracksController < ApplicationController
  # GET /tracks
  # GET /tracks.xml
  layout 'standard'

  def index
    if(params[:id])
      @assemblage = Assemblage.find_by_id(params[:id])
      @tracks = @assemblage.tracks
    else
      redirect_non_admins and return
      @tracks = Track.find(:all)
    end
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @tracks }
    end
  end

  # GET /tracks/1
  # GET /tracks/1.xml
  def show
    @track = Track.find(params[:id])
    rescue ActiveRecord::RecordNotFound 
      logger.error("Attempt to access invalid track #{params[:id]}") 
      flash[:notice] = "Track #{params[:id]} doesn't exist." 
      redirect_to :action => :index 
    else
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @track }
    end
  end

  # GET /tracks/new
  # GET /tracks/new.xml
  def new
    @track = Track.new
    @track.assemblage_id = params[:id]

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @track }
    end
  end

  # GET /tracks/1/edit
  def edit
    @track = Track.find(params[:id])
    @from = params[:from] || ''
    respond_to do |format|
      format.html {}
      format.js {render :action => 'edit',:layout => false} 
    end
  end

  # POST /tracks
  # POST /tracks.xml
  def create
    @track = Track.new(params[:track])
    respond_to do |format|
      if @track.save
        flash[:notice] = 'Track was successfully created.'
        format.html { redirect_to(@track) }
        format.xml  { render :xml => @track, :status => :created, :location => @track }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @track.errors, :status => :unprocessable_entity }
      end
    end
  end

  def create_new
    assemblage = Assemblage.find(params[:id])
    number_text = 'Track ' + (assemblage.tracks.length + 1).to_s
    assemblage.tracks.create(
      :title => number_text,
      :track_type => 'dance')
      flash[:notice] = 'Track was successfully created.'
    redirect_to(:controller => 'assemblages', :action => 'present', :id => params[:id])
  end

  # PUT /tracks/1
  # PUT /tracks/1.xml
  def update
    @track = Track.find(params[:id])
    params[:track]= Hash.new
    params[:track][:title]=params[:title]
    params[:track][:color]=params[:color]
    params[:track][:track_type] = params[:track_type]
    if @track.update_attributes(params[:track])
      @track.insert_at(params[:position])
      flash[:notice] = 'Track was successfully updated.'
      redirect_to(params[:from] || {:controller => 'assemblages', :action => 'present', :id => session[:assemblage_id]})
    else
      render :action => "edit"
    end
  end

  # DELETE /tracks/1
  # DELETE /tracks/1.xml
  def destroy
    @track = Track.find(params[:id])
    @track.destroy

    respond_to do |format|
      format.html { redirect_to(:action => 'index',:id => session[:assemblage_id]) }
      format.xml  { head :ok }
    end
  end
  
end
