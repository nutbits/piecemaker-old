class AssemblagesController < ApplicationController
  
  helper_method :stripe_width, :number_of_stripes, :stripe_seconds, :total_duration_seconds,:pre_roll, :piece_duration
  before_filter :get_block_from_params, :only => [:break_cue,:group,:create_blocklist_from_block,:erase,:update_block]
  before_filter :get_assemblage_from_params, :only => [:show,:edit,:update,:quick_cap,:group_after,:performer_filter,:group_all,:ungroup_all,:undo_one,:clear]
  
  def get_assemblage_from_params
    @assemblage = Assemblage.find(params[:id])
  end
  def get_block_from_params
    @block = Block.find(params[:id])
  end
  def clear
    @assemblage.blocks.each do |block|
      block.destroy
    end
    redirect_to :action => 'present', :id => @assemblage.id
  end
  def stripe_width
    @sw ||= (@time_scale > 1)? stripe_seconds/@time_scale :  stripe_seconds*@time_scale.abs
  end
  def number_of_stripes      
    @nos ||= (total_duration_seconds / stripe_seconds)+1 #come up with a better solution for integer division
  end
  def stripe_seconds
    STRIPE_SECONDS[@time_scale]
  end
  def total_duration_seconds
    @tds ||= pre_roll + piece_duration + @assemblage.post_roll
  end
  def pre_roll
    @assemblage.pre_roll
  end
  def piece_duration
    @assemblage.piece_duration
  end
  
  def index
    redirect_to :action => :list
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
      if (set_current_piece(params[:id]))
        @assemblages = current_piece.assemblages
      else
        unset_current_piece
        @assemblages = Assemblage.all
      end
      render :layout => "standard"
  end

  def dragtest
    set_numbers
  end
  def show
    render :layout => "standard"
  end

  def new
    @assemblage = Assemblage.new
    render :layout => "standard"
  end

  def edit
    render :layout => "standard"
  end

  def update
    params[:assemblage][:grid] ||= 0
    params[:assemblage][:overlap_check] ||= 0
    params[:assemblage][:constrained] ||= 0
    params[:assemblage][:track_undos] ||= 0
    params[:assemblage][:track_cues] ||= 0
    params[:assemblage][:warning] ||= 0
    params[:assemblage][:piece_duration] = params_to_duration(:piece_duration)
    params[:assemblage][:pre_roll] = params_to_duration(:pre_roll)
    params[:assemblage][:post_roll] = params_to_duration(:post_roll)
    params[:assemblage][:start_time] = params_to_duration(:start_time)
    if @assemblage.update_attributes(params[:assemblage])
      unless @assemblage.track_undos
        @assemblage.clear_undo_cue
      end
      flash[:notice] = 'Assemblage was successfully updated.'
      redirect_to :action => 'show', :id => @assemblage
    else
      render :action => 'edit'
    end
  end
  def config
    @configuration = Configuration.first

    render :layout => false
  end
  def update_config
    @configuration = Configuration.first
    @configuration.update_attributes(params[:configuration])
    flash[:notice] = "config updated"
    set_numbers
    redirect_to :action => 'present', :id => session[:assemblage_id]
  end
  def report
    @assemblage = Assemblage.find(params[:id])
    @array = @assemblage.report
  end
  def destroy
    assemblage = Assemblage.find(params[:id])
    pieceid = assemblage.piece_id
    assemblage.destroy
    redirect_to :action => 'list', :id => pieceid
  end
  


#### Methods for presenting and manipuating blocks
  def quick_cap
  end
  def set_numbers(id = nil)
    
    if id
      session[:assemblage_id] = id
        if current_user.last_assemblage_id != id
          current_user.last_assemblage_id = id
          current_user.save
        end
    elsif current_user.last_assemblage_id
      session[:assemblage_id] = current_user.last_assemblage_id
    end
    
    ratio = params[:ratio] || session[:scale] || 1 # 3 is  the index of scale 1
    @time_scale = session[:scale] = ratio.to_i
    
    @showpanel = params[:showpanel] || session[:showpanel] || 'true'
    session[:showpanel] = @showpanel
    
    @flatten = params[:flatten] || session[:flatten] || 'false'
    session[:flatten] = @flatten == 'false'? false : true  
    
    track_height = params[:track_height] || session[:track_height] || 60
    session[:track_height] = @track_height = track_height.to_i #need
    
    time_display = params[:time_display] || session[:time_display] || 'ms'
    session[:time_display] = time_display
    @hours = session[:time_display] == 'hm'? true : false
    @seconds = session[:time_display] == 'ms'? true : false
  
    @assemblage = Assemblage.find_by_id(session[:assemblage_id],:include => [{:tracks => {:blocks => :assemblage}},:piece])
    if @assemblage.track_cues
      @blocks = Block.find_all_by_assemblage_id(@assemblage.id,:conditions => ['is_clone = ?', false], :include =>[:scene,{:cuers => [:track,:cuees]},{:cuees => :cuers},:track] )
    else
      @blocks = Block.find_all_by_assemblage_id(@assemblage.id,:conditions => ['is_clone = ?', false], :include =>[:scene,:track] )
    end
    @scenes = Scene.find_all_by_assemblage_id(@assemblage.id, :include =>[{:blocks => :track}])
    @blocklists = Blocklist.find(:all, :conditions => "piece_id = #{@assemblage.piece_id}", :order => "title",:include => :blocks)
    session[:piece_id] = @assemblage.piece_id
  end

  def break_cue 
=begin
  TODO put break_cue into model and test
=end
    cue_er = @block.break_cueing
    set_numbers
    @block = Block.find(params[:id])
    @grouped_blocks = [@block,cue_er]
    respond_to do |format|
      format.html {}
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
    
  end

  def group
    @block.grouped = @block.grouped == 0 ? 1 : 0
    @block.save
    set_numbers
    @grouped_blocks = [@block]
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
    
  end

  def group_after
    respond_to do |format|
      format.html {}
      format.js {render :action => 'group_after',:layout => false} 
    end
  end
  
  def group_all
    @grouped_blocks = []
    after = params[:after] ? params[:after].to_i * 60 : -1000
    @assemblage = Assemblage.find(params[:id])
    for block in @assemblage.blocks
      if block.start_time > after && block.grouped == 0
        block.grouped = 1
        block.save
        @grouped_blocks << block
      elsif block.start_time <= after && block.grouped == 1
        block.grouped = 0
        block.save
        @grouped_blocks << block
      end
    end
    set_numbers
    respond_to do |wants|
      wants.html { redirect_to :action => 'present', :id => params[:id] }
      wants.js {render :action => 'grouped_blocks',:layout => false}
    end
    
  end
  
  def ungroup_all
    @grouped_blocks = []
    @assemblage = Assemblage.find(params[:id])
    for block in @assemblage.blocks
      if block.grouped == 1
        block.grouped = 0
        block.save
        @grouped_blocks << block
      end
    end
    set_numbers
    respond_to do |wants|
      wants.html { redirect_to :action => 'present', :id => params[:id] }
      wants.js {render :action => 'grouped_blocks',:layout => false}
    end
      
  end
  
  def performer_filter
    respond_to do |format|
      format.html {}
      format.js {render :action => 'performer_filter',:layout => false} 
    end
  end
  def generate
    assemblage = Assemblage.find(params[:id])
    number = assemblage.generate
    flash[:notice] = "generated #{number.to_s}"
    redirect_to :action => 'present', :id => params[:id] 
  end
  def enhance
    assemblage = Assemblage.find(params[:id])
    assemblage.enhance()
    flash[:notice] = "enhanced"
    redirect_to :action => 'present', :id => params[:id]
  end
  
  def present
                  
    redirect_to :action => 'list' unless set_numbers(params[:id].to_i)
    flash[:notice] = "Showing: #{params[:search].join(', ')}" if params[:search]
     if session[:flatten]
       @assemblage.tracks.delete_if do |track|
         blks = track.blocks.reject{|x| x.is_clone}
         blks.delete_if{|block| params[:search] && (block.cast & params[:search]).length == 0}
         blks.length == 0
       end
     end
     respond_to do |format|
       format.html {}
       format.pdf do
         prawnto :prawn => {
                       :page_layout => :landscape, 
                       :page_size => [@assemblage.tracks.length*35+100,stripe_width*(number_of_stripes+1)+180]}
        end 
     end
    
  end

  def array_from_block_move
    params[:id].split('m')
  end 
  def start_time_from_params
    array_from_block_move[1].to_i
  end

  def track_shift_amount_from_params
    @tsa ||= array_from_block_move[2].to_i
  end
  
  def block_id_from_params
    bd = array_from_block_move[0].split('-')
    block_id = bd[1].to_i
  end
  def landed_on_scene_track(block,shift_amount)
    if block.track.position + shift_amount == block.assemblage.tracks.length + 1
        scenes = Scene.find_all_by_assemblage_id(block.assemblage_id).reject{|x| !x.start_time}
        if scenes.length > 0
          scene = scenes.select{|x| x.start_time <= start_time_from_params && x.end_time >= start_time_from_params}
        end
    end
    if scene && scene.length > 0
      scene.last.id
    else
      false
    end
    
  end
  def landed_on_block(block)
    landed_track = block.find_track(track_shift_amount_from_params)
    if landed_track
      possible_blocks = Block.find_all_by_track_id(landed_track.id,:conditions => "start_time < #{start_time_from_params}")
      short_list = possible_blocks.select{|x| x.end_time > start_time_from_params}

    end
    if short_list && short_list.length > 0
      short_list.last
    else
      false
    end
  end

  def cue_up_drag
    thisblock = Block.find(block_id_from_params)
    receiving_block = landed_on_block(thisblock)
    set_numbers
    
    if receiving_block 
      if receiving_block.start_time <= thisblock.start_time + 10
        if receiving_block.start_time <= thisblock.start_time
          flash[:notice] = "Cued up '#{receiving_block.title}' to '#{thisblock.title}'."
        else
          thisblock.start_time = receiving_block.start_time
          thisblock.save
          flash[:notice] = "Cued up '#{receiving_block.title}' to '#{thisblock.title}'. #{thisblock.title} time adjusted."
        end
        receiving_block.cuees << thisblock
        receiving_block.save
        @grouped_blocks = [thisblock,receiving_block]
        @grouped_blocks += receiving_block.cuer_brothers
      else
        flash[:notice] = "You can't cue a block in the past!"
        @grouped_blocks = [thisblock]
      end
    else
      flash[:notice] = "Couldn't cue up blocks, nothing to cue up with"
      @grouped_blocks = [thisblock]
    end
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
  end
  
  def create_blocklist_from_block
    blocklist = Blocklist.new
    blocklist.title = @block.title
    blocklist.duration = @block.duration
    blocklist.div_class = @block.div_class || TRACK_TYPES[@block.track_id][:color]
    blocklist.event_type = @block.track.track_type
    blocklist.piece_id = @block.assemblage.piece_id
    blocklist.cast = @block.cast
    blocklist.save
    @block.blocklist = blocklist
    @assemblage = @block.assemblage
    @scenes = @assemblage.scenes
    @blocklists = Blocklist.find(:all, :conditions => "piece_id = #{@assemblage.piece_id}", :order => "title")
    respond_to do |wants|
      wants.html {redirect_to :controller => "assemblages", :action => 'present', :id => @assemblage.id} 
      wants.js{render :action => 'create_from_block',:layout => false}
    end
  end

  
  def update_start_time
    thisblock = Block.find(block_id_from_params)
    time_difference = start_time_from_params - thisblock.start_time     
    landed = landed_on_scene_track(thisblock,track_shift_amount_from_params)
    if landed
      @grouped_blocks = thisblock.perform_all_actions_and_return_affected_blocks({:type => :group,:act => 'update',:push => :push}) do |x|
        x.give_block_to_scene(landed)
      end
    else
      @grouped_blocks = thisblock.perform_all_actions_and_return_affected_blocks({:type => :group,:act => 'update',:push => :push}) do |x|
        x.convert_track_id(track_shift_amount_from_params)
        x.move_by_difference(time_difference)
      end
    end
    set_numbers
    ids = @grouped_blocks.map{|x| x.id}
    @grouped_blocks = @blocks.select{|x| ids.include?(x.id)}
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
  end

  def show_ancestors
    @event = Event.find(params[:id])
    set_numbers
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :action => 'show_ancestors',:layout => false} 
    end
  end
  def update_scene_start_time
    @scene = Scene.find(block_id_from_params)
    time_difference = start_time_from_params - @scene.start_time 
    @grouped_blocks = @scene.blocks.first.perform_all_actions_and_return_affected_blocks({:type => :scene,:act => 'update',:push => :push}) do |x|
      x.convert_track_id(0)
      x.move_by_difference(time_difference)
    end
    set_numbers
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :controller => 'assemblages', :action => 'grouped_blocks',:layout => false} 
    end
  end


  def update_duration
    array_from_box = params[:id].split('-')
    block_id = array_from_box[1].to_i
    new_duration = array_from_box[2].to_i
    block = Block.find(block_id)   
    @grouped_blocks = block.perform_all_actions_and_return_affected_blocks({:type => :group,:act => 'update',:push => :push}) do |x|
      x.set_duration(new_duration)
    end
    set_numbers
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
  end
  def stuff_params_from_blocklist(blocklist)
      params[:block][:cast] = blocklist.cast
      params[:block][:title] = blocklist.title
      params[:block][:event_id] = blocklist.event_id
      params[:block][:duration] = blocklist.duration ###
      params[:block][:div_class] = blocklist.div_class
      params[:block][:dependent] = blocklist.dependent
      params[:block][:blocklist_id] = blocklist.id
  end
  def update_block
    old_scene = @block.scene
    undo_type = params[:create] == 'true' ? 'create' : 'update'
    params[:block][:is_clone] = false
    params[:block][:scene_id] = nil if params[:block][:scene_id] == '0'
    if params[:bl] && params[:bl] != 'none'
      blocklist = Blocklist.find(params[:bl])
      stuff_params_from_blocklist(blocklist)
      params[:block][:assemblage_id] = session[:assemblage_id]
      params[:block][:created_by] = current_user.login
    else 
      params[:block][:dependent] ||= false
      params[:block][:cast] ||= []
      params[:block][:div_class] = params[:div_class] || @block.div_class
    end
    @grouped_blocks = @block.perform_all_actions_and_return_affected_blocks({:act => undo_type,:push => :push}) do |x|
      x.attributes = params[:block]
    end
    #block.attributes = params[:block]
    #block.save
    set_numbers
    #@grouped_blocks = [block]
    flash[:notice] = 'Block was successfully updated.'
      respond_to do |format|
        format.html {redirect_to :controller => 'assemblages',:action => 'present', :id => @block.assemblage_id }
        format.js {render :action => 'grouped_blocks',:layout => false} 
      end
  end

  def add
    @grouped_blocks = Block.add_block_from_params(params,session[:assemblage_id],current_user)
    set_numbers
    @blocklists = Blocklist.find(:all, :conditions => "piece_id = #{@assemblage.piece_id}", :order => "title")
    @tracks = Track.find_all_by_assemblage_id(session[:assemblage_id], :order => 'position',:include => :blocks)
     respond_to do |format|
       format.html {redirect_to :action => 'present', :id => @assemblage.id }
       format.js {render :action => 'grouped_blocks',:layout => false} 
     end

  end


  def erase
    @grouped_blocks = @block.perform_all_actions_and_return_affected_blocks({:act => 'delete',:push => :push}) do |x|
      x.make_deleted
    end
    set_numbers
    @grouped_blocks += @block.cuer_brothers
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end

  end

  def create #refactor
    params[:assemblage][:piece_duration] = params_to_duration(:piece_duration)
    params[:assemblage][:pre_roll] = params_to_duration(:pre_roll)
    params[:assemblage][:post_roll] = params_to_duration(:post_roll)
    params[:assemblage][:start_time] = params_to_duration(:start_time)
    @assemblage = Assemblage.new(params[:assemblage])
    @assemblage.piece_id = params[:piece_id]
    @assemblage.created_by = current_user.login
    @assemblage.block_list = Array.new
    if @assemblage.save
      if(@assemblage.constrained)
        TRACK_TYPES.each_with_index do |track_type,index|
          next if track_type[:class] == 'scene'
          track = Track.create(
          :title => track_type[:name],
          :track_type => track_type[:class],
          :assemblage_id => @assemblage.id,
          :position => index + 1
          )
        end
      else
        FREE_TRACK_TYPES.each_with_index do |track_type,index|
          next if track_type[:class] == 'scene'
          track = Track.create(
          :title => track_type[:name],
          :track_type => track_type[:class],
          :assemblage_id => @assemblage.id,
          :position => index + 1
          )
        end
      end
      flash[:notice] = 'Assemblage was successfully created.'
      redirect_to :action => 'list', :id => params[:piece_id]
    else
      render :action => 'new'
    end
  end
  
  def save_as_version
    old_assemblage = Assemblage.find(params[:id])
    @assemblage = old_assemblage.perform_clone(current_user.login)
    if params[:return_to] == 'use'
      redirect_to :controller => 'assemblages', :action => 'present', :id => @assemblage.id
    else
      redirect_to :controller => 'assemblages', :action => "list", :id => @assemblage.piece_id
    end
  end
  
  def listing_by_track
    params[:performer] ||= 'Everyone'
    session[:assemblage_id] = params[:id]
    @assemblage = Assemblage.find(session[:assemblage_id])
    @tracks = @assemblage.tracks
    @tracks.sort_by{|x| x.position}
    
  end
  def listing_by_time_and_type
    @warnings = []
    @assemblage = Assemblage.find(session[:assemblage_id])
    @blocks = @assemblage.blocks.reject{|x| x.is_clone}.sort_by{|x| x.start_time}
    if @assemblage.constrained
    else
      render :action => 'listing_by_time'
    end
  end
  def listing_by_time
    @assemblage = Assemblage.find(session[:assemblage_id])
    @blocks = @assemblage.blocks.sort_by{|x| x.start_time}
    
  end

  def listing_by_scene
    @warnings = []
    @assemblage = Assemblage.find(session[:assemblage_id])
    @scenes = @assemblage.used_scenes.sort_by { |x| x.start_time }
    if (WARNINGS.length > 0) && (@assemblage.warning == true)
      @assemblage.blocks.each do |block|
        if WARNINGS.include?(TRACK_TYPES[block.track_id][:class]) && block.scene
          @warnings << block
        end
      end
      @warnings = @warnings.sort_by{|x| x.start_time}
    end

    render :action => 'listing_by_scene', :layout => 'assemblages'
  end

  def group_actions
    #all I need to do here is find the first grouped block
    all_blocks = Block.find_all_by_assemblage_id(session[:assemblage_id],:order => 'start_time')
    some = all_blocks.select{|x| x.grouped == 1}
    if some.length > 0
     @grouped_blocks = Block.do_group_actions(some,params)
    else
      @grouped_blocks=[]
    end
    set_numbers
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => @assemblage.id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
  end
  def create_scene
    set_numbers
    scene = Scene.create(params[:scene])
    scene.assemblage_id = params[:id]
    scene.save
    @grouped_blocks = []
    @scenes = [scene]
    @all_scenes = scene.assemblage.scenes
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => scene.assemblage_id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
  end
  def update_scene #this just updates the scene's title
    set_numbers
    scene = Scene.find(params[:id])
    scene.update_attributes(params[:scene])
    @grouped_blocks = scene.blocks
    @scenes = [scene]
    @all_scenes = scene.assemblage.scenes
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => scene.assemblage_id }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
  end

  
  
############################################# iffy stuff  
  
  def clear_undo_cue
    assemblage = Assemblage.find(params[:id])
    assemblage.clear_undo_cue
    @grouped_blocks = []
    @scenes =[]
    set_numbers
    respond_to do |format|
      format.html {redirect_to :action => 'present', :id => params[:id] }
      format.js {render :action => 'grouped_blocks',:layout => false} 
    end
  end

    def undo_one
      flash[:notice] = "There's Nothing to Undo"
      @grouped_blocks = []
      @assemblage = Assemblage.find(params[:id])
      undo = @assemblage.block_undos.sort_by{|x| x.position}.last
      if undo
        undo_info = undo.perform_undo
        @grouped_blocks = undo_info[0]
        @erase = undo_info[1]
        flash[:notice] = "Undo Completed #{undo.position} (#{undo.edit_type})"
        undo.destroy
      end
      set_numbers      
      respond_to do |format|
        format.html {redirect_to :controller => 'assemblages',:action => 'present', :id => params[:id] }
        format.js {render :action => 'grouped_blocks',:layout => false} 
      end
    end
    

    def update_blocklist
      @blocklist = Blocklist.find(params[:id])
      params[:blocklist][:cast] ||= []
      params[:blocklist][:dependent] ||= false
      params[:blocklist][:duration] = params_to_duration
      params[:blocklist][:div_class] = params[:div_class] || @blocklist.div_class
      params[:from]=params[:from].gsub('.js','')
      if @blocklist.update_attributes(params[:blocklist])
        if @blocklist.dependent
           deps = @blocklist.blocks.dependent
            if deps.length > 0
              @grouped_blocks = deps[0].perform_all_actions_and_return_affected_blocks({:type => :blocklist,:act => 'update',:push => false}) do |x|
                x.get_attributes_from_blocklist(@blocklist)
              end
            else
              @grouped_blocks = []
            end
        else
          @grouped_blocks = []
        end

        set_numbers
        flash[:notice] = 'Blocklist was successfully updated.'
        respond_to do |format|
          format.html {redirect_to(params[:from] || {:controller => 'assemblages', :action => 'present', :id => @blocklist.assemblage_id})}
          format.js {render :action => 'grouped_blocks',:layout => false} 
        end

      else
        render :action => 'edit'
      end
    end
    
end
