class ScenesController < ApplicationController
  layout 'assemblages'

  def new
    @assemblage = Assemblage.find(params[:id])
    respond_to do |wants|
      wants.html {  }
      wants.js { render :action => 'new', :layout => false}
    end
  end
  def delete
    @scene = Scene.find(params[:id])
    @scene.destroy
    respond_to do |wants|
      wants.html {  }
      wants.js { render :text => ''}
    end
  end
  def edit
    @scene = Scene.find(params[:id])
    @assemblage = @scene.assemblage
    respond_to do |wants|
      wants.html {  }
      wants.js { render :action => 'edit', :layout => false}
    end
  end
  def update
    @scene = Scene.find(params[:id])
    @scene.update_attributes(params[:scene])
    redirect_to :action => 'list', :id => @scene.assemblage_id
  end
  def create
    assemblage = Assemblage.find(session[:assemblage_id])
    if @scene = Scene.create(
      :title => params[:scene][:title],
      :assemblage_id => session[:assemblage_id])
      flash[:notice] = 'Scene Created'
    else
      flash[:notice] = "Couldn't Create Scene"
    end
    @scenes = assemblage.scenes
    respond_to do |wants|
      wants.html { redirect_to :controller => 'assemblages', :action => 'present', :id => session[:assemblage_id] }
      wants.js { render :partial => 'create_scene',:layout => false}
    end
    
  end
  
  
end
