class BlocklistsController < ApplicationController
  layout 'standard'
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    redirect_non_admins('create_events',home_url) and return
    if session[:assemblage_id]
      @assemblage = Assemblage.find(session[:assemblage_id])
      @blocklists = @assemblage.piece.blocklists
    else
      redirect_non_admins('group_admin',home_url) and return
      @blocklists = Blocklist.find(:all)
    end
  end
  def list_all
    @blocklists = Blocklist.paginate(:all,:page => params[:page],:order => 'id')
    render :action => 'list'
  end

  def show
    @blocklist = Blocklist.find(params[:id])
  end

  def new
    @blocklist = Blocklist.new
    @blocklist.cast = Array.new
    @assemblage = Assemblage.find(session[:assemblage_id])
    respond_to do |format|
      format.html {}
      format.js {render :action => 'new',:layout => false} 
    end
  end


  def create
    params[:blocklist][:cast] ||= []
    params[:blocklist][:duration] = params_to_duration()
    params[:blocklist][:div_class] = CLASS_COLORS[params[:blocklist][:event_type].to_sym]
    blocklist = Blocklist.new(params[:blocklist])
    blocklist.piece_id = params[:piece_id]
    if blocklist.save
      @assemblage = Assemblage.find(session[:assemblage_id])
      @blocklists = Blocklist.find(:all, :conditions => "piece_id = #{params[:piece_id]}", :order => "title")
      flash[:notice] = 'Blocklist was successfully created.'
      respond_to do |format|
        format.html {redirect_to(params[:from] || {:controller => 'assemblages', :action => 'present', :id => session[:assemblage_id]})}
        format.js {render :action => 'create_from_block',:layout => false}
      end
    else
      render :action => 'new'
    end
  end

  def edit
    @blocklist = Blocklist.find(params[:id])
    @assemblage = Assemblage.find(session[:assemblage_id])
    respond_to do |format|
      format.html {}
      format.js {render :action => 'edit',:layout => false} 
    end
  end


  def destroy
    blocklist = Blocklist.find(params[:id])
    blocklist.destroy
    redirect_to :action => 'list'
  end
  
end
