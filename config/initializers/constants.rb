RED = 'f98'
YELLOW = 'fc8'
GREEN = '8fb'
TURQUOISE = '70bbd0'
BLUE = '88f'
GREY = 'a0a0a0'
CLASS_COLORS = {
  :light => YELLOW,
  :cues => RED,
  :sound => GREEN,
  :text => TURQUOISE,
  :dance => BLUE,
  :scenes => GREY
}
#RED = '920'
#YELLOW = '960'
#GREEN = '094'
#BLUE = '009'
TRACK_TYPE_LIST = %w[cues light sound text dance]
TRUNCATE_LENGTH = 300
FOUND_TEXT_REPLACEMENT_STRING = '<span class="found">\1</span>'
TRACK_TYPES = [
 { :name => 'cues-1' , :color => CLASS_COLORS[:cues], :accepts => true, :class => 'cues' ,:precedance => 1 },# 0
# { :name => 'cues-2'   , :color => CLASS_COLORS[:cues], :accepts => false, :class => 'cues' ,:precedance => 1 },# 1
 { :name => 'light'  , :color => CLASS_COLORS[:light], :accepts => true, :class => 'light',:precedance => 2 },# 2
 { :name => 'sound-1', :color => CLASS_COLORS[:sound], :accepts => true, :class => 'sound',:precedance => 3 },# 3
 { :name => 'sound-2', :color => CLASS_COLORS[:sound], :accepts => true, :class => 'sound',:precedance => 3 },# 4
 { :name => 'text-1', :color => CLASS_COLORS[:text], :accepts =>  true, :class => 'text',:precedance => 4 },# 5
 { :name => 'dance-1', :color => CLASS_COLORS[:dance], :accepts =>  true, :class => 'dance',:precedance => 5 },# 6
 { :name => 'dance-2', :color => CLASS_COLORS[:dance], :accepts =>  true, :class => 'dance',:precedance => 6 },# 7
 { :name => 'dance-3', :color => CLASS_COLORS[:dance], :accepts =>  true, :class => 'dance',:precedance => 7 },# 8
 { :name => 'scenes' , :color => CLASS_COLORS[:scenes],:accepts =>  false, :class => 'scene',:precedance => 0} # 9
  ]
FREE_TRACK_TYPES = [
   { :name => '!' , :color => CLASS_COLORS[:cues], :accepts => true, :class => 'cues' ,:precedance => 1 },# 0
  # { :name => 'cues-2'   , :color => CLASS_COLORS[:cues], :accepts => false, :class => 'cues' ,:precedance => 1 },# 1
   { :name => '2'  , :color => CLASS_COLORS[:light], :accepts => true, :class => 'light',:precedance => 2 },# 2
   { :name => '3', :color => CLASS_COLORS[:sound], :accepts => true, :class => 'sound',:precedance => 3 },# 3
   { :name => '4', :color => CLASS_COLORS[:sound], :accepts => true, :class => 'sound',:precedance => 3 },# 4
   { :name => '5', :color => CLASS_COLORS[:text], :accepts =>  true, :class => 'text',:precedance => 4 },# 5
   { :name => '6', :color => CLASS_COLORS[:dance], :accepts =>  true, :class => 'dance',:precedance => 5 },# 6
   { :name => '7', :color => CLASS_COLORS[:dance], :accepts =>  true, :class => 'dance',:precedance => 6 },# 7
   { :name => '8', :color => CLASS_COLORS[:dance], :accepts =>  true, :class => 'dance',:precedance => 7 },# 8
    ]
WARNINGS = ['cues']
BLOCKS_WITH_CAST = ['dance','text']
#161 17 53
#53 58 144
#77 8 103
#197 107 35
#126 101 101
#61 17 123
#0 96 75

STRIPE_SECONDS ={
  -8 => 15,
  -4 => 15,
  -2 => 30,
  1 => 60,
  2 => 120,
  4 => 120,
  6 => 300
}

GRID_RHYTHM = {
  -8=> 4,
  -4=> 4,
  -2=> 2,
   1=> 5,
   2=> 5,
   4=> 5,
   6=> 3
}


