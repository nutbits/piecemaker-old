class RemoveIsDefAndIsAnnot < ActiveRecord::Migration
  def self.up
    remove_column :events, :is_annotation
    remove_column :events, :is_definitive
  end

  def self.down
    add_column :events, :is_definitive, :boolean,  :default => false
    add_column :events, :is_annotation, :boolean,  :default => false
  end
end
