class AddPrefsToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :notes_on, :boolean, :default => true
    add_column :users, :markers_on, :boolean, :default => true
    add_column :users, :refresh_pref, :integer, :default => 0
    add_column :users, :truncate, :string, :default => 'more'
    add_column :users, :inherit_cast, :boolean, :default => false
    add_column :users, :last_assemblage_id, :integer
    add_column :users, :last_login, :timestamp
  end

  def self.down
    remove_column :users, :last_login
    remove_column :users, :inherit_cast
    remove_column :users, :last_assemblage_id
    remove_column :users, :truncate
    remove_column :users, :refresh_pref
    remove_column :users, :notes_on
    remove_column :users, :markers_on
  end
end