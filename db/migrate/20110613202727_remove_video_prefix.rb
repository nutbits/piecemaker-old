class RemoveVideoPrefix < ActiveRecord::Migration
  def self.up
    remove_column :pieces, :video_prefix
  end

  def self.down
    add_column :pieces, :video_prefix, :string, :default => "DVD"
  end
end
