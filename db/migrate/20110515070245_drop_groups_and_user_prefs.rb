class DropGroupsAndUserPrefs < ActiveRecord::Migration
  def self.up
    drop_table :groups
    drop_table :user_prefs
  end

  def self.down
  end
end
