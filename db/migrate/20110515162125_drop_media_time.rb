class DropMediaTime < ActiveRecord::Migration
  def self.up
    remove_column :events, :media_time
    remove_column :events, :video_parent_id
  end

  def self.down
    add_column :events, :video_parent_id, :integer
    add_column :events, :media_time, :integer
  end
end
