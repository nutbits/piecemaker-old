class AddAssemblageStuffToConfig < ActiveRecord::Migration
  def self.up
    add_column :configurations, :desired_on_time, :integer
    add_column :configurations, :min_entrances, :integer
    add_column :configurations, :max_entrances, :integer
    add_column :configurations, :min_entrance_time, :integer
    add_column :configurations, :max_entrance_time, :integer
  end

  def self.down
    remove_column :configurations, :max_entrance_time
    remove_column :configurations, :min_entrance_time
    remove_column :configurations, :max_entrances
    remove_column :configurations, :column_name
    remove_column :configurations, :desired_on_time
  end
end