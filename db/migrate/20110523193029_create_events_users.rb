class CreateEventsUsers < ActiveRecord::Migration
  def self.up
    create_table :events_users,:id => false, :force => true do |t|
      t.integer :event_id
      t.integer :user_id
    end
    add_index :events_users, :event_id
    add_index :events_users, :user_id
  end

  def self.down
    remove_index :events_users, :event_id
    remove_index :events_users, :user_id
    drop_table :events_users
  end
end