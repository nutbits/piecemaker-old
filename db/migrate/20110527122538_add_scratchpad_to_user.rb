class AddScratchpadToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :scratchpad, :text
  end

  def self.down
    remove_column :users, :scratchpad
  end
end