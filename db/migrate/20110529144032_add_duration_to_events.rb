class AddDurationToEvents < ActiveRecord::Migration
  def self.up
    add_column :events, :dur, :integer, :default => nil
  end

  def self.down
    remove_column :events, :dur
  end
end