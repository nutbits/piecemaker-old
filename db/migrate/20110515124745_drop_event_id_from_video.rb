class DropEventIdFromVideo < ActiveRecord::Migration
  def self.up
    remove_column :videos, :event_id
    remove_column :videos, :piece_id
  end

  def self.down
  end
end
