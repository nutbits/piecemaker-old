class AddUpdatedAt < ActiveRecord::Migration
  def self.up
    add_column :assemblages, :updated_at, :timestamp
    add_column :blocklists, :updated_at, :timestamp
    add_column :castings, :updated_at, :timestamp
  end

  def self.down
    remove_column :castings, :updated_at
    remove_column :blocklists, :updated_at
    remove_column :assemblages, :updated_at
  end
end