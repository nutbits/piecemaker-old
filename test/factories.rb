Factory.define :scene do |s|
  s.title  'Test Scene'
  s.assemblage_id  1
end
Factory.define :block do |b|
  
end
Factory.define :blocklist do |b|
  
end
Factory.define :assemblage do |a|
  
end

Factory.define :piece do |p|
  p.title 'opus 1'
end
Factory.define :event do |e|
  e.title 'event 1'
end
Factory.define :tag do |e|
  e.name 'tag 1'
end
Factory.define :user do |e|
  e.login 'ubi'
end

Factory.define :performer do |e|
  e.short_name 'ubi'
end