require 'test_helper'
require 'assemblages_controller'

# Re-raise errors caught by the controller.
class AssemblagesController; def rescue_action(e) raise e end; end

class AssemblagesControllerTest < ActionController::TestCase
fixtures :users
  def setup
    @controller = AssemblagesController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end
  # Replace this with your real tests.
  should 'redirect if not logged in' do
    get 'list'
    assert_response :redirect
  end
  
  logged_in_as "super_user" do
      should 'list assemblages if logged in' do
        get 'list'
        assert_response :success
        assert_equal 3, assigns(:assemblages).size
        assert_equal current_user.login, "David"
      end
      should 'destroy assembalge' do
        post 'destroy' , :id => 1
      end
  end
  
end
