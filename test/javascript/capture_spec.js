require("spec_helper.js");
require("../../public/javascripts/jquery-form.js");
require("../../public/javascripts/jquery-scroll.js");
require("../../public/javascripts/jquery-timer.js");
require("../../public/javascripts/jquery-jedit.js");
require("../../public/javascripts/application.js");
require("../../public/javascripts/capture.js");

Screw.Unit(function(){
  describe("Your application javascript", function(){
    it("does something", function(){
      expect("hello").to(equal, "hello");
    });

    it("accesses the DOM from fixtures/application.html", function(){
      expect($('.select_me').length).to(equal, 2);
    });
  });
});
