# == Schema Information
#
# Table name: assemblages
#
#  id             :integer(4)      not null, primary key
#  piece_id       :integer(4)
#  created_at     :datetime
#  created_by     :string(255)
#  block_list     :text
#  title          :string(255)
#  piece_duration :integer(4)      default(3600)
#  pre_roll       :integer(4)      default(0)
#  start_time     :integer(4)      default(0)
#  post_roll      :integer(4)      default(0)
#  grid           :boolean(1)      default(TRUE)
#  overlap_check  :boolean(1)      default(FALSE)
#  constrained    :boolean(1)      default(FALSE)
#  warning        :boolean(1)      default(FALSE)
#  track_cues     :boolean(1)      default(TRUE)
#  track_undos    :boolean(1)      default(TRUE)
#

'test_helper'

class AssemblageTest < ActiveSupport::TestCase
  Assemblage.send(:public, *Assemblage.protected_instance_methods)
  # Replace this with your real tests.
  context "Assemblage Class Method" do
    setup do
      @as1 = Assemblage.create(
        :title => 'as1'
      )
      @sc1 = Scene.create(
        :assemblage_id => @as1.id
      )
      @block = Block.create(
        :assemblage_id => @as1.id,
        :start_time => 10
      )
    end
    should 'return empty array if no used scenes' do
      assert_equal @as1.used_scenes, []
    end
    should 'return array of used scenes' do
      @sc1.blocks << @block
      assert_equal @as1.used_scenes, [@sc1]
    end
  end
end

