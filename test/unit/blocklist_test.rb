# == Schema Information
#
# Table name: blocklists
#
#  id         :integer(4)      not null, primary key
#  created_at :datetime
#  created_by :string(255)
#  event_id   :string(255)
#  title      :string(255)
#  duration   :integer(4)      default(120)
#  div_class  :string(255)     default("e9f")
#  left       :integer(4)      default(400)
#  used       :integer(4)      default(0)
#  piece_id   :integer(4)
#  cast       :text
#  dependent  :boolean(1)      default(FALSE)
#  event_type :string(255)     default("dance")
#

require 'test_helper'

class BlocklistTest < ActiveSupport::TestCase
  #fixtures :blocklists
  Blocklist.send(:public, *Blocklist.protected_instance_methods)
  context "blocklist create from block" do
    setup do
      @event = Event.new
      @event.title = 'tom'
      @event.performers = ['alan','jodi']
      @event.piece_id = 4
      @event.save
      @blocklist = Blocklist.create_from_events([@event])
    end
    should 'create blocklist from event title' do
      assert @blocklist[0].title == @event.title
    end
    should 'create blocklist from event performers' do
      assert @blocklist[0].cast == @event.performers
    end
    should 'create blocklist from event piece_id' do
      assert @blocklist[0].piece_id == @event.piece_id
    end
    should 'create blocklist from event id' do
      assert @blocklist[0].event_id == @event.id
    end
  end
  context 'blocklist as blocks in assemblage' do
    setup do
      @p = Factory.create(:piece)
      @as1 = Factory.create(:assemblage)
      @as2 = Factory.create(:assemblage)
      @bl1 = Factory.create(:blocklist,:title => ' music_note')
      @bl2 = Factory.create(:blocklist,:title => 'bl2title')
      @b1 = Factory.create(:block,:start_time => 100,:duration => 20,:title => 'title1',:dependent => false)
      @b2 = Factory.create(:block,:start_time => 100,:duration => 20,:title => 'title2')
      @b3 = Factory.create(:block,:start_time => 100,:duration => 20,:title => 'title3',:dependent => true)
      @bl1.blocks << @b1
      @as1.blocks << @b1
      @bl1.blocks << @b3
      @as1.blocks << @b3
      @bl2.blocks << @b2
      @as2.blocks << @b2
    end
    
    should 'free up dependent blocks on destroy' do
      @bl1.destroy
      @b3 = Block.find(@b3.id)
      assert_equal @b3.dependent, false
    end
    
    should 'return true if has a block in assemblage' do
      assert @bl1.has_blocks_in_assemblage?(@as1.id)
      assert @bl2.has_blocks_in_assemblage?(@as2.id)
    end
    should 'return false if no block in assemblage' do
      assert !@bl2.has_blocks_in_assemblage?(@as1.id)
      assert !@bl1.has_blocks_in_assemblage?(@as2.id)
    end
    should 'update dependent' do
      assert_equal('title3', @b3.title)
      @bl1.update_dependent_blocks
      @b3 = Block.find(@b3.id)
      assert_equal(' music_note', @b3.title)
    end
    should 'not update non dependent' do
      assert_equal('title1', @b1.title)
      @bl1.update_dependent_blocks
      @b1 = Block.find(@b1.id)
      assert_equal('title1', @b1.title)
    end
    should 'get permanently unused right' do
      assert @bl1.permanently_unused?
      assert !@bl2.permanently_unused?
    end
  end
end

