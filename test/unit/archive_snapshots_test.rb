require 'test_helper'

class ArchiveSnapshotsTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  ArchiveSnapshot.send(:public, *ArchiveSnapshot.protected_instance_methods)
  # Replace this with your real tests.
  context "compare snapshots" do
    setup do
      @as1 = ArchiveSnapshot.create(
      :snapshot => %w[one two three four five]
      )
      @as2 = ArchiveSnapshot.create(
      :snapshot => %w[two three four five six seven]
      )
    end
    should 'diff lists' do
      diffs = ArchiveSnapshot.diff_lists(%w[one two three],%w[three two one])
      assert_equal diffs[:missing_from_first], %w[]
      assert_equal diffs[:missing_from_second], %w[]
    end
    should 'work with nil too' do
      diffs = ArchiveSnapshot.diff_lists(%w[one two three],nil)
      assert_equal diffs[:missing_from_first], %w[]
      assert_equal diffs[:missing_from_second], %w[one two three]
    end
    should 'give difference in list' do
      daffs = ArchiveSnapshot.diff_lists(%w[one two],%w[three two])
      assert_equal daffs[:missing_from_first], %w[three]
      assert_equal daffs[:missing_from_second], %w[one]
    end
    should 'list missing from first' do
      diffs = ArchiveSnapshot.diff_snapshots(@as1,@as2)
      assert_equal diffs[:missing_from_first], %w[six seven]
      assert_equal diffs[:missing_from_second], %w[one]
    end
    should 'list nothing if equal' do
      @as1.snapshot = %w[two three four five six seven]
      @as1.save
      diffs = ArchiveSnapshot.diff_snapshots(@as1,@as2)
      assert_equal diffs[:missing_from_first], %w[]
      assert_equal diffs[:missing_from_second], %w[]
    end
  end
end
