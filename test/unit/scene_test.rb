# == Schema Information
#
# Table name: scenes
#
#  id            :integer(4)      not null, primary key
#  title         :string(255)
#  assemblage_id :integer(4)
#  created_at    :datetime
#  updated_at    :datetime
#

require 'test_helper'

class SceneTest < ActiveSupport::TestCase
  Scene.send(:public, *Scene.protected_instance_methods)
  
  context "a Scene" do
    setup do
      @s = Factory(:scene, :title => 's')
      @s2 = Factory(:scene,:title => 's2')
      @s3 = Factory(:scene,:title => 's3')
    end
    should 'return a correct title' do
      assert @s.title == 's'
    end
    should 'return nil for start time if scene has no blocks' do
      assert @s.start_time == nil
    end
    should 'return nil for end time if scene has no blocks' do
      assert @s.end_time == nil
    end
    should 'return nil for duration if scene has no blocks' do
      assert @s.duration == nil
    end
    context 'a Scene with blocks' do
      setup do
        @s.blocks.build.stubs(:start_time => 10,:duration =>10,:cast=>['bill'])
        @s.blocks.build.stubs(:start_time => 20,:duration =>35,:cast=>['bill','biff','bozz'])
        @s.blocks.build.stubs(:start_time => 40,:duration =>10,:cast=>['george','candy'])
      end
      should 'have a block' do
        assert @s.blocks.length == 3
      end
      should 'get correct start time' do
        assert @s.start_time == 10
      end
      should 'get correct end time' do
        assert @s.end_time == 55
      end
      should 'get correct duration' do
        assert @s.duration == 45
      end
      should 'get correct cast' do
        assert @s.cast == ['biff','bill','bozz','candy','george']
      end
    end
    context 'find scenes for a group of blocks' do
      setup do
        @b1 = @s.blocks.build(:title => 'b1')
       @b2 = @s.blocks.build(:title => 'b2')
       @b3 = @s.blocks.build(:title => 'b3')
       @b4 = @s2.blocks.build(:title => 'b4')
       @b5 = @s2.blocks.build(:title => 'b5')
       @b6 = @s2.blocks.build(:title => 'b6')        
      end
      should 'return a scene if given a block' do
        scenes = Scene.scenes_for([@b1])
        assert_equal [@s], scenes
      end
      should 'return 1 scene if give two different blocks in same scene' do
        scenes = Scene.scenes_for([@b1,@b2])
        assert_equal [@s], scenes
      end
      should 'return 2 scenes if give two different blocks in different scenes' do
        scenes = Scene.scenes_for([@b1,@b4]).sort_by{|x| x.title}
        assert_equal [@s,@s2], scenes
      end
      should 'return 2 scenes if give several different blocks in  2different scenes' do
        scenes = Scene.scenes_for([@b1,@b4,@b2,@b5]).sort_by{|x| x.title}
        assert_equal [@s,@s2], scenes
      end
    end
    
    context 'find current scene based on block start_time' do

        setup do
          @as = Assemblage.new
          @as.save
          @scene1 = Scene.new
          @scene1.assemblage_id = @as.id
          @scene1.title = 'scene'
          @scene1.save
          @block1 = Block.new
          @block1.start_time = 10
          @block1.duration = 20
          @block1.assemblage_id = @as.id
          @block1.scene_id = @scene1.id
          @block1.save
          @block2 = Block.new
          @block2.start_time = 15
          @block2.assemblage_id = @as.id
          @block2.save
          @block3 = Block.new
          @block3.start_time = 150
          @block3.assemblage_id = @as.id
          @block3.save
        end
        should 'Scene.scene_for_block return nil if no correct scene' do
          assert_equal Scene.scene_id_for_block(@block3), nil
        end
        should 'return correct scene Scene.scene_for_block if there is a scene' do
          assert_equal Scene.scene_id_for_block(@block2), @scene1.id          
        end

    end
    
    context 'get a scenes number' do
      setup do
        @as = Assemblage.new
        @as.save
        @scene1 = Scene.new
        @scene1.assemblage_id = @as.id
        @scene1.title = 'scene1'
        @scene1.save
        @scene2 = Scene.new
        @scene2.assemblage_id = @as.id
        @scene2.title = 'scene2'
        @scene2.save
        @block1 = Block.new
        @block1.start_time = 10
        @block1.duration = 20
        @block1.assemblage_id = @as.id
        @block1.scene_id = @scene1.id
        @block1.save
        @block2 = Block.new
        @block2.start_time = 35
        @block2.duration = 20
        @block2.assemblage_id = @as.id
        @block2.scene_id = @scene2.id
        @block2.save
      end
      should 'get correct scene number' do
        assert_equal(@scene1.scene_number, 1)
        assert_equal(@scene2.scene_number, 2)
      end
    end
    
    context 'get a scenes z-index' do
      setup do
        @as = Assemblage.new
        @as.save
        @scene1 = Scene.new
        @scene1.assemblage_id = @as.id
        @scene1.title = 'scene1'
        @scene1.save
        @scene2 = Scene.new
        @scene2.assemblage_id = @as.id
        @scene2.title = 'scene2'
        @scene2.save
        @scene3 = Scene.new
        @scene3.assemblage_id = @as.id
        @scene3.title = 'scene3'
        @scene3.save
        @block1 = Block.new
        @block1.start_time = 10
        @block1.duration = 20
        @block1.assemblage_id = @as.id
        @block1.scene_id = @scene1.id
        @block1.save
        @block2 = Block.new
        @block2.start_time = 15
        @block2.duration = 20
        @block2.assemblage_id = @as.id
        @block2.scene_id = @scene2.id
        @block2.save
        @block3 = Block.new
        @block3.start_time = 45
        @block3.duration = 20
        @block3.assemblage_id = @as.id
        @block3.scene_id = @scene3.id
        @block3.save
      end
      should 'get correct z-index' do
        assert_equal(@scene1.z_index, 100)
        assert_equal(@scene2.z_index, 101)
        assert_equal(@scene3.z_index, 100)
      end
    end
    
    context 'non-cloned blocks' do
      setup do
        @as = Assemblage.new
        @as.save
        @scene1 = Scene.new
        @scene1.assemblage_id = @as.id
        @scene1.title = 'scene1'
        @scene1.save
        @block1 = Block.new
        @block1.start_time = 10
        @block1.duration = 20
        @block1.assemblage_id = @as.id
        @block1.scene_id = @scene1.id
        @block1.is_clone = false
        @block1.save
        @block2 = Block.new
        @block2.start_time = 15
        @block2.duration = 20
        @block2.assemblage_id = @as.id
        @block2.scene_id = @scene1.id
        @block2.is_clone = false
        @block2.save
        @block3 = Block.new
        @block3.start_time = 45
        @block3.duration = 20
        @block3.assemblage_id = @as.id
        @block3.scene_id = @scene1.id
        @block3.is_clone = true
        @block3.save
      end
      should 'give correct non-cloned-blocks' do
        assert_equal(@scene1.non_cloned_blocks.length,2)
      end
    end
    
  end
end

