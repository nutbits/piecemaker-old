# == Schema Information
#
# Table name: blocks
#
#  id            :integer(4)      not null, primary key
#  created_at    :datetime
#  created_by    :string(255)
#  event_id      :integer(4)
#  title         :string(255)
#  parent_id     :string(255)
#  updated_at    :datetime
#  start_time    :integer(4)
#  duration      :integer(4)
#  left          :integer(4)
#  div_class     :string(255)     default("e9f")
#  description   :string(255)
#  modified_by   :string(255)
#  grouped       :integer(4)      default(0)
#  blocklist_id  :integer(4)
#  assemblage_id :integer(4)
#  track_id      :integer(4)
#  cast          :text
#  z_index       :integer(4)      default(100)
#  cued_by_id    :integer(4)
#  is_clone      :boolean(1)      default(FALSE)
#  dependent     :boolean(1)      default(FALSE)
#  block_type    :string(255)
#  scene_id      :integer(4)
#

require 'test_helper'

class BlockTest < ActiveSupport::TestCase
  Block.send(:public, *Block.protected_instance_methods)
  
  
  context 'block and cuers' do
    setup do
      @a = Factory.create(:assemblage)
      @b1 = Factory.create(:block,:start_time => 10,:duration => 20)
      @b2 = Factory.create(:block,:start_time => 20,:duration => 20)
      @b3 = Factory.create(:block,:start_time => 30,:duration => 20)
      @a.blocks << @b1
      @a.blocks << @b2
      @a.blocks << @b3
    end
    should 'find real cuer normally' do
      @b1.cuees << @b2
      assert_equal @b2.cuer, @b1
    end
    should 'find last of several cuers' do
      @b1.cuees << @b3
      @b2.cuees << @b3
      assert_equal @b3.cuer, @b2
    end
    should 'find reject clone cuer' do
      @b1.cuees << @b3
      @b2.cuees << @b3
      @b2.is_clone = true
      @b2.save
      assert_equal @b3.cuer, @b1
    end
    context 'cuer brothers' do
      setup do
        @a = Factory.create(:assemblage)
        @b1 = Factory.create(:block,:start_time => 10,:duration => 20)
        @b2 = Factory.create(:block,:start_time => 20,:duration => 20)
        @b3 = Factory.create(:block,:start_time => 30,:duration => 20)
        @b4 = Factory.create(:block,:start_time => 40,:duration => 20)
        @a.blocks << @b1
        @a.blocks << @b2
        @a.blocks << @b3
        @a.blocks << @b3
        
        @b2.cuers << @b1
        @b4.cuers << @b2
        @b4.cuers << @b3
      end
      should 'break cue' do
        assert_equal(@b2.cuers,[@b1])
        @b2.break_cueing
        bb = Block.find(@b2.id)
        assert_equal(bb.cuers,[])
      end
      should 'get cuer brothers' do
        #should return self, cuees, and cuers of cuees
        assert_equal @b2.cuer_brothers.length,4
      end
    end
    context 'real cuees' do
      setup do
        @b4 = Factory.create(:block,:start_time => 30,:duration => 20,:is_clone => true)
        @b5 = Factory.create(:block,:start_time => 35,:duration => 20)
        @b6 = Factory.create(:block,:start_time => 05,:duration => 20)
        @a.blocks << @b4
        @a.blocks << @b5
        @b1.cuees << @b2
        @b1.cuees << @b3
        @b1.cuees << @b4
        @b1.cuees << @b5
        @b1.cuees << @b6
        @b2.cuees << @b6
        @b5.cued_by_id = @b5.id
        @b5.save
      end
      should 'show early cuees' do
        assert @b1.has_early_cuees?
      end
      should 'show late cuer' do
        assert @b6.has_late_cuer?
        assert !@b5.has_late_cuer?
      end
      should 'find real cuees' do
        assert_equal @b1.cuees,[@b2,@b3,@b4,@b5,@b6]
      end
      should 'reject clone, self referential, strange id' do
        assert_equal @b1.real_cuees,[@b2,@b3]
      end
    end

  end
  
  context 'overlap check' do
    setup do
      @a = Factory.create(:assemblage,:overlap_check => true)
      @a2 = Factory.create(:assemblage)
      @b1 = Factory.create(:block)
      @b2 = Factory.create(:block)
      @a.blocks << @b1
      @a2.blocks << @b2
    end
    should 'report overlap check' do
      assert @b1.check_for_overlaps?
      assert !@b2.check_for_overlaps?
    end
  end
  context 'a block instance' do
    setup do
      @block1 = Factory(:block)
      @block2 = Factory(:block)
      @block3 = Factory(:block)
      @block4 = Factory(:block)
      @block5 = Factory(:block)
      @block1.grouped = 1
      @block1.title = '1'
      @block1.assemblage_id = 7
      @block1.save
      
      @block2.grouped = 1
      @block2.title = '2'
      @block2.assemblage_id = 7
      @block2.save
      
      @block3.grouped = 0
      @block3.title = '3'
      @block3.assemblage_id = 7
      @block3.save
      
      @block4.grouped = 1
      @block4.title = '4'
      @block4.assemblage_id = 7
      @block4.save
      
      @block5.grouped = 1
      @block5.title = '5'
      @block5.assemblage_id = 8
      @block5.save
    end
    should 'make deleted and undeleted' do
      assert !@block1.is_clone
      @block1.make_deleted
      assert @block1.is_clone
      @block1.make_undeleted
      assert !@block1.is_clone
    end
    should 'give block to scene' do
      @block1.give_block_to_scene(7)
      assert_equal(7, @block1.scene_id)
    end
    should 'get copiable columns' do
      assert Block.copyable_columns.include?('start_time')
      assert !Block.copyable_columns.include?('id')
    end
    should  'detect overlap changes' do
      a=[1,2,3]
      b=[2]
      assert_equal @block1.overlap_changes(a,b), [1,3]
    end
    should 'return correct end time' do
      @block1.start_time = 100
      @block1.duration = 45
      assert_equal @block1.end_time, 145
    end
    should 'set duration' do
      @block1.duration = 45
      assert_equal @block1.duration, 45
      @block1.set_duration(112)
      assert_equal @block1.duration, 112
    end
    context 'finding overlaps' do    
      setup do
        @block1.start_time = 100
        @block1.duration = 45   
        @block1.cast = ['a','b','c']     
        @block2.start_time = 90
        @block2.duration = 11
        @block2.cast = ['a','b','c']
        @block3.start_time = 144
        @block3.duration = 15
        @block3.cast = ['a','b','c']
        @block4.start_time = 44
        @block4.duration = 15
        @block4.cast = ['a','b','c']
        @block5.start_time = 150
        @block5.duration = 15
        @block5.cast = ['a','b','c']
      end
      should 'report correct overlapped blocks' do
          all_blocks = [@block1]
          assert_equal @block1.overlapped(all_blocks,0), []
          all_blocks = [@block1,@block2,@block3,@block4,@block5]
          assert_equal @block1.overlapped(all_blocks,0), [@block2,@block3]
          assert_equal @block1.overlapped(all_blocks,10), [@block2,@block3,@block5]
        
      end
      should 'report cast overlapped' do
        all_blocks = [@block1,@block2,@block3,@block4,@block5]
        assert_equal @block1.castoverlapped(all_blocks,0), [@block2,@block3]
        @block3.cast=['d','e']
        assert_equal @block1.castoverlapped(all_blocks,0), [@block2]
      end
      should 'return empty array if block is frozen' do
        all_blocks = [@block1,@block2,@block3,@block4,@block5]
        @block1.destroy
        assert_equal @block1.castoverlapped(all_blocks,0), []
      end
      should 'return empty array if block is new_record' do
        all_blocks = [@block1,@block2,@block3,@block4,@block5]
        @bll = Block.new
        assert_equal @bll.castoverlapped(all_blocks,0), []
      end

      should 'return empty array if no castoverlapped blocks exist' do
        all_blocks = [@block1,@block2,@block4,@block5]
        @block2.cast = ['g','h']
        assert_equal @block1.castoverlapped(all_blocks,0), []
      end
    end

    should 'return correct group partners array' do
      grp2 = @block1.group_partners_from_array([@block1,@block2,@block3,@block4,@block5]).map{|x| x.title}.sort
      assert_equal grp2, ['1','2','4']
    end

    context 'expand' do
      setup do
        @block1.start_time = 0
        @block2.start_time = 10
        @block3.start_time = 15
        @block4.start_time = 100
      end
      should 'expand correctly if no difference' do
        @block1.expand(0,50)
        assert_equal @block1.start_time,0
      end
      should 'expand correctly if difference' do
        @block2.expand(0,1.5)
        assert_equal @block2.start_time,15
      end
      should 'expand correctly with reference' do
        @block3.expand(10,2)
        assert_equal @block3.start_time,20
      end
    end
    context 'unswallow' do
      setup do
        @block1.start_time = 0
        @block1.duration   = 20
        @block1.z_index    = 101
        @block2.start_time = 10
        @block2.duration   = 20
        @block2.z_index    = 105
        @block3.start_time = 15
        @block3.duration   = 20
        @block3.z_index    = 101
        @block4.start_time = 100
        @block4.duration   = 20
        @block4.z_index    = 109
        @all_blocks = [@block1,@block2,@block3,@block4]
        Block.fix_z_indexes(@all_blocks)
      end
      
      should 'set z indexes of first block to 100' do
        assert_equal @block1.z_index,100
      end
      should 'set z indexes of second block to 101' do
        assert_equal @block2.z_index,101
      end
      should 'set z indexes of third block to 102' do
        assert_equal @block3.z_index,102
      end
      should 'set z indexes of fourth block to 100' do
        assert_equal @block4.z_index,100
      end
      should 'not change z index if not needed' do
        assert_equal @block1.change_z_index_if_needed(100),false
        assert_equal @block1.z_index, 100
      end
      should 'change z index if needed' do
        assert_equal @block1.change_z_index_if_needed(110),true
        assert_equal @block1.z_index, 110
      end
    end

  end
  context 'convert track id tests' do
    setup do
      @track1 = Track.new
      @track1.position = 1
      @track1.track_type = 'cues'
      @track1.assemblage_id = 2
      @track1.save
      
      @track2 = Track.new
      @track2.position = 2
      @track2.track_type = 'light'
      @track2.assemblage_id = 2
      @track2.save
      
      @track3 = Track.new
      @track3.position = 3
      @track3.track_type = 'dance'
      @track3.assemblage_id = 3
      @track3.save
      
      @track4 = Track.new
      @track4.position = 3
      @track4.track_type = 'dance'
      @track4.assemblage_id = 2
      @track4.save
      
      @track5 = Track.new
      @track5.position = 4
      @track5.track_type = 'dance'
      @track5.assemblage_id = 2
      @track5.save
      
      @as = Assemblage.create
      @bl = Block.new
      @bl.assemblage_id = @as.id
      @bl.track_id = @track1.id
      @bl.save
    end
    should 'convert track id' do
      @bl.convert_track_id(2)
      assert_equal @bl.track_id, @track4.id
    end
    should 'convert track id twice' do
      @bl.convert_track_id(2)
      @bl.convert_track_id(-1)
      assert_equal @bl.track_id, @track2.id
    end
    should 'keep same track if moved by too much' do
      @bl.convert_track_id(4)
      assert_equal @bl.track_id, @track1.id
    end
    should 'return same id if convert by 0' do
      @bl.track_id = 144
      @bl.save
      @bl.convert_track_id(0)
      assert_equal @bl.track_id,144
    end
    should 'return 1 affected track if convert by 0' do
      @bl.reset_ati
      @bl.track_id = 144
      @bl.save
      assert_equal @bl.convert_track_id(0),[144]
    end
    should 'return 2 affected tracks if converted' do
      @bl.reset_ati
      assert_equal @bl.convert_track_id(2),[@track1.id,@track4.id]
    end
    should 'return 1 affected track if converted by too much' do
      @bl.reset_ati
      assert_equal @bl.convert_track_id(4),[@track1.id]
    end
    should 'keep same track if assemblage constrained and moved to wrong track class' do
      @as.constrained = true
      @as.save
      @bl.reset_ati
      @bl.track_id = @track1.id
      @bl.convert_track_id(1)
      assert_equal @bl.track_id,@track1.id
    end
    should 'move if constrained and moved to a correct track class' do
      @as.constrained = true
      @as.save
      @bl.reset_ati
      @bl.track_id = @track5.id
      @bl.convert_track_id(-1)
      assert_equal @bl.track_id,@track4.id
    end
    should 'return 1 affected track if convert by 0 constrained' do
      @as.constrained = true
      @as.save
      @bl.reset_ati
      @bl.track_id = 144
      @bl.save
      assert_equal @bl.convert_track_id(0),[144]
    end
    should 'return 1 affected track if converted by too much constrained' do
      @as.constrained = true
      @as.save
      @bl.reset_ati
      @bl.track_id = @track4.id
      assert_equal @bl.convert_track_id(4),[@track4.id]
    end
    should 'return 2 affected tracks if converted constrained' do
      @as.constrained = true
      @as.save
      @bl.reset_ati
      @bl.track_id = @track4.id
      assert_equal @bl.convert_track_id(1).sort,[@track4.id,@track5.id]
    end
  end
  context 'test move_by_difference' do
    should 'move block corectly' do
      @bl = Block.new
      @bl.start_time = 155
      @bl.move_by_difference(15)
      assert_equal @bl.start_time, 170
    end
  end
  context 'round start time test' do
    should 'return rounded start value' do
      @bl = Block.new
      @bl.start_time = 155
      @bl.round_start_time(15)
      assert_equal @bl.start_time, 150
    end
  end

end

