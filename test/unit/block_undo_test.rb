# == Schema Information
#
# Table name: block_undos
#
#  id            :integer(4)      not null, primary key
#  blocks        :text
#  position      :integer(4)
#  assemblage_id :integer(4)
#  edit_type     :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  clone_data    :text
#

require 'test_helper'

class BlockUndoTest < ActiveSupport::TestCase
  Casting.send(:public, *BlockUndo.protected_instance_methods)
  context 'a block_undo instance' do
    setup do
      @bu = BlockUndo.create
      @b1 = Block.create
      @b2 = Block.create
      @b3 = Block.create
      @b4 = Block.create
      @bu.clone_data = [{'id' => @b1.id,'data' => {"one" => 1}},{'id' => @b2.id,'data' => {'two' => '2'}}]
      @bu.save
    end
    should 'correctly serialize blocks' do
      assert_equal @bu.clone_data.length, 2
    end

    
  end
end
