require 'test_helper'

class BlocksHelperTest < ActionView::TestCase
  context 'a block instance' do
    setup do
      @as = Assemblage.create(:constrained => false)
      @block = Block.create(:assemblage_id => @as.id)     
    end
    should 'return true for needs cast if not constrained' do
      assert block_needs_cast?
    end
  end
end