require 'rubygems'
require 'spork'

ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'test_help'

require "webrat"
require 'shoulda'
#require 'mocha'
require 'factory_girl'
#require 'authenticated_test_helper'
require 'timecop'
#require File.dirname(__FILE__) + "/factories"
Spork.prefork do

  Webrat.configure do |config|
    config.mode = :rails
  end

  class ActiveSupport::TestCase

    # Sets the current person in the session from the person fixtures.
    def self.logged_in_as(person, &block)
      context "logged in as #{person}" do
        setup do
          @request.session[:user_id] = users(person).id
        end

        yield
      end
    end
    # Transactional fixtures accelerate your tests by wrapping each test method
    # in a transaction that's rolled back on completion.  This ensures that the
    # test database remains unchanged so your fixtures don't have to be reloaded
    # between every test method.  Fewer database queries means faster tests.
    #
    # Read Mike Clark's excellent walkthrough at
    #   http://clarkware.com/cgi/blosxom/2005/10/24#Rails10FastTesting
    #
    # Every Active Record database supports transactions except MyISAM tables
    # in MySQL.  Turn off transactional fixtures in this case; however, if you
    # don't care one way or the other, switching from MyISAM to InnoDB tables
    # is recommended.
    self.use_transactional_fixtures = true

    # Instantiated fixtures are slow, but give you @david where otherwise you
    # would need people(:david).  If you don't want to migrate your existing
    # test cases which use the @david style and don't mind the speed hit (each
    # instantiated fixtures translates to a database query per test method),
    # then set this back to true.
    self.use_instantiated_fixtures  = false
    fixtures :all
    # Add more helper methods to be used by all tests here...
  end


end

Spork.each_run do
  # This code will be run each time you run your specs.

end

# --- Instructions ---
# - Sort through your spec_helper file. Place as much environment loading
#   code that you don't normally modify during development in the
#   Spork.prefork block.
# - Place the rest under Spork.each_run block
# - Any code that is left outside of the blocks will be ran during preforking
#   and during each_run!
# - These instructions should self-destruct in 10 seconds.  If they don't,
#   feel free to delete them.
#




